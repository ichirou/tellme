//
//  AccountTableViewCell.swift
//  TellMe
//
//  Created by Richmond Ko on 29/09/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {

    @IBOutlet var accountTypeLabel: UILabel!
    @IBOutlet var accountNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
