//
//  AccountsTableViewController.swift
//  TellMe
//
//  Created by Richmond Ko on 29/09/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Parse

class AccountsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var delegate: PassDataBackDelegate?
    var dashboardDelegate: DashboardDelegate?
    var accounts = [PFObject]()
    var loadingHelper = LoadingHelper()
    var selectedAccount = String()
    var sourceViewController = String()
    var selectedAccountRow = -1
    var selectedRow = -1
    
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var navigationBar: UINavigationBar!
    @IBOutlet var deleteButton: UIBarButtonItem!
    @IBOutlet var editButton: UIBarButtonItem!
    
    //# MARK: - Custom Methods
    func checkIfAccountIsBeingUsedThenDelete(selectedAccountIndex: Int) {
        loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        
        let query = PFQuery(className: "Expense")
        query.whereKey("account_pointer", equalTo: accounts[selectedAccountIndex])
        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        
        query.findObjectsInBackground(block: { (objects, error) in
            if error != nil {
                DispatchQueue.main.async {
                    let errorMessage = ErrorHelper.parseError(defaultMessage: "Finding account to delete failed, please try again later.", error: error)
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    
                    AlertHelper.createAlert(title: "Error", message: errorMessage, view: self)
                }
            }
            else if (objects?.count)! > 0 {
                DispatchQueue.main.async {
                    AlertHelper.createAlert(title: "Cannot delete account", message: "The selected account is currently being used, edit or delete first the related transactions", view: self)
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                }
            }
            else {
                self.deleteAccount(selectedTableRow: selectedAccountIndex)
            }
        })
    }
    
    func deleteAccount(selectedTableRow: Int) {
        let accountQuery = PFQuery(className: "Account")
        accountQuery.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        accountQuery.whereKey("objectId", equalTo: accounts[selectedTableRow].objectId!)
        
        accountQuery.findObjectsInBackground(block: { (objects, error) in
            if error != nil {
                DispatchQueue.main.async {
                    let errorMessage = ErrorHelper.parseError(defaultMessage: "Finding account to delete failed, please try again later.", error: error)
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    
                    AlertHelper.createAlert(title: "Error", message: errorMessage, view: self)
                }
            }
            else {
                if let objects = objects {
                    for object in objects {
                        object.deleteInBackground(block: { (success, error) in
                            if error != nil {
                                DispatchQueue.main.async {
                                    let errorMessage = ErrorHelper.parseError(defaultMessage: "Delete account failed, please try again later.", error: error)
                                    
                                    AlertHelper.createAlert(title: "Error", message: errorMessage, view: self)
                                }
                            }
                            else if success {
                                DispatchQueue.main.async {
                                    AlertHelper.createAlert(title: "Success", message: "Account deleted.", view: self)
                                }
                            }
                            DispatchQueue.main.async {
                                self.loadingHelper.stopActivityIndicator(uiView: self.view)
                            }
                            self.getAccounts()
                        })// end delete object in background
                    }
                }
            }
            
        })// end find objects in background
    }
    
    func getAccounts() {
        loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        //find all accounts owned by current logged in user
        let accountQuery = PFQuery(className: "Account")
        accountQuery.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        accountQuery.order(byDescending: "name")
        
        accountQuery.findObjectsInBackground { (objects, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    let errorMessage = ErrorHelper.parseError(defaultMessage: "Get accounts failed, please try again later.", error: error)
                    
                    AlertHelper.createAlert(title: "Error", message: errorMessage, view: self)
                }
            }
            else {
                self.accounts.removeAll()
                
                if let accounts = objects {
                    for account in accounts {
                        self.accounts.append(account)
                    }
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                }
            }
        }
    }
    
    //# MARK: - IBAction Methods
    @IBAction func editAccount(_ sender: AnyObject) {
        performSegue(withIdentifier: "showEditAccount", sender: self)
    }
    
    @IBAction func deleteAccount(_ sender: AnyObject) {
        checkIfAccountIsBeingUsedThenDelete(selectedAccountIndex: selectedRow)
        editButton.isEnabled = false
        deleteButton.isEnabled = false
    }
    
    @IBAction func cancel(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //# MARK: - View Methods
    override func viewDidAppear(_ animated: Bool) {
        getAccounts()
        editButton.isEnabled = false
        deleteButton.isEnabled = false

        if sourceViewController == "menu" {
            navigationBar.topItem?.title = "Manage Accounts"
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if sourceViewController == "menu" {
            dashboardDelegate?.reloadDashboard()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    //# MARK: - TableView Methods
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accounts.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AccountTableViewCell

        cell.accountNameLabel.text = accounts[indexPath.row]["name"] as? String
        cell.accountTypeLabel.text = accounts[indexPath.row]["type"] as? String
        
        if sourceViewController == "addTransaction" {
            if indexPath.row == selectedAccountRow {
                cell.accessoryType = .checkmark
            }
        }
        else if sourceViewController == "menu" {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        
        if sourceViewController == "addTransaction" {
            selectedAccount = accounts[indexPath.row]["name"] as! String
            
            delegate?.setSelectedAccountObject(account: accounts[indexPath.row])
            print(accounts[indexPath.row])
            delegate?.setSelectedAccountButtonTitle(value: selectedAccount)
            delegate?.setSelectedAccountRow(value: indexPath.row)
            delegate?.setSelectAccountButtonTextColor(color: UIColor.black)

            self.dismiss(animated: true, completion: nil)
        }
        else if sourceViewController == "menu" {
            cell?.accessoryType = .checkmark
            selectedRow = indexPath.row
            editButton.isEnabled = true
            deleteButton.isEnabled = true
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        
        if sourceViewController == "menu" {
            cell?.accessoryType = .none
        }
    }
    
    internal func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if sourceViewController == "menu" {
            return true
        }
        else {
            return false
        }
    }
    
    //# MARK: - Segue Methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEditAccount" {
            let destinationViewController = segue.destination as! AddAccountViewController
            destinationViewController.editMode = true
            destinationViewController.account = accounts[selectedRow]
        }
    }

}
