//
//  AddAccountViewController.swift
//  TellMe
//
//  Created by Richmond Ko on 29/09/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Parse

class AddAccountViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    let accountTypePickerData = ["Cash", "Charge"]
    var loadingHelper = LoadingHelper()
    var editAccountLoadingHelper = LoadingHelper()
    var account = PFObject(className: "Account")
    var editMode = false
    
    @IBOutlet var addAccountButton: UIButton!
    @IBOutlet var navigationBar: UINavigationBar!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var accountTypeTextField: UITextField!
    @IBOutlet var accountNameTextField: UITextField!
    
    //# MARK: - Custom Methods
    func editAccount() {
        editAccountLoadingHelper.showActivityIndicatory(uiView: self.view, message: "Updating...")
        
        let query = PFQuery(className: "Account")
        query.getObjectInBackground(withId: account.objectId!) { (object: PFObject?, error) in
            if error != nil {
                DispatchQueue.main.async {
                    ErrorHelper.setErrorLabel(defaultMessage: "Edit account failed, please try again later.", errorLabel: self.errorLabel, error: error)
                    self.editAccountLoadingHelper.stopActivityIndicator(uiView: self.view)
                }
            }
            else if let account = object {
                account["name"] = self.accountNameTextField.text 
                account["type"] = self.accountTypeTextField.text 
                
                account.saveInBackground(block: { (success, error) in
                    if error != nil {
                        DispatchQueue.main.async {
                            ErrorHelper.setErrorLabel(defaultMessage: "Edit account failed, please try again later.", errorLabel: self.errorLabel, error: error)
                            self.editAccountLoadingHelper.stopActivityIndicator(uiView: self.view)
                        }
                    }
                    else if success {
                        DispatchQueue.main.async {
                            self.editAccountLoadingHelper.stopActivityIndicator(uiView: self.view)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                })
            }
        }
    }
    
    func executeAddAccount() {
        loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        errorLabel.text = ""
        
        let newAccount = PFObject(className: "Account")
        
        let existingAccounts = PFQuery(className: "Account")
        existingAccounts.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        existingAccounts.whereKey("name", equalTo: accountNameTextField.text!)
        
        existingAccounts.findObjectsInBackground(block: { (objects, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    ErrorHelper.setErrorLabel(defaultMessage: "Searching existing accounts failed, please try again later.", errorLabel: self.errorLabel, error: error)
                }

            }
            else if (objects?.count)! > 0 {
                self.errorLabel.text = "Account already exists!"
            }
            else {
                newAccount["owner"] = PFUser.current()?.objectId
                newAccount["name"] = self.accountNameTextField.text
                newAccount["type"] = self.accountTypeTextField.text
                
                newAccount.saveInBackground(block: { (success, error) in
                    if success {
                        DispatchQueue.main.async {
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    else if error != nil {
                        DispatchQueue.main.async {
                            self.loadingHelper.stopActivityIndicator(uiView: self.view)
                            ErrorHelper.setErrorLabel(defaultMessage: "Add account failed, please try again later.", errorLabel: self.errorLabel, error: error)
                        }
                    }
                    DispatchQueue.main.async {
                        self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    }
                })
            }
        })
    }
    
    //# MARK: - IBAction Methods
    @IBAction func cancel(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addAccount(_ sender: AnyObject) {
        if accountNameTextField.text == "" {
            errorLabel.text = "Account name is required"
        }
        else {
            if editMode {
                editAccount()
            }
            else {
                executeAddAccount()
            }
        }
    }
    
    //# MARK: - PickerView Methods
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return accountTypePickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return accountTypePickerData[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        accountTypeTextField.text = accountTypePickerData[row]
    }
    
    //# MARK: - TextField Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    //# MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let accountTypePicker = UIPickerView()
        accountTypePicker.delegate = self
        accountTypePicker.dataSource = self
        accountTypeTextField.inputView = accountTypePicker
        
        if editMode {
            //set navigation bar title and button title
            navigationBar.topItem?.title = "Edit Account"
            addAccountButton.setTitle("Update Account", for: UIControlState.normal)
            
            accountNameTextField.text = account["name"] as! String?
            accountTypeTextField.text = account["type"] as! String?
        }
        
    }

    override func viewDidAppear(_ animated: Bool) {
        BorderHelper.addBottomBorder(view: accountNameTextField, color: UIColor.gray)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
