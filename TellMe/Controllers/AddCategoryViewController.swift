//
//  AddCategoryViewController.swift
//  TellMe
//
//  Created by Richmond Ko on 02/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Parse

class AddCategoryViewController: UIViewController {

    let loadingHelper = LoadingHelper()
    let editCategoryLoadingHelper = LoadingHelper()
    var category = PFObject(className: "Category")
    var editMode = false
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var categoryNameTextField: UITextField!
    @IBOutlet var navigationBar: UINavigationBar!
    @IBOutlet var addCategoryButton: UIButton!
    
    //# MARK: - Custom Methods
    func editCategory() {
        editCategoryLoadingHelper.showActivityIndicatory(uiView: self.view, message: "Updating...")
        
        let query = PFQuery(className: "Category")
            
        query.getObjectInBackground(withId: category.objectId!) { (object: PFObject?, error) in
            if error != nil {
                DispatchQueue.main.async {
                    ErrorHelper.setErrorLabel(defaultMessage: "Edit category failed, please try again later.", errorLabel: self.errorLabel, error: error)
                    self.editCategoryLoadingHelper.stopActivityIndicator(uiView: self.view)
                }
            }
            else if let category = object {
                category["name"] = self.categoryNameTextField.text
                category["canonicalName"] = self.categoryNameTextField.text?.lowercased()
                
                category.saveInBackground(block: { (success, error) in
                    if error != nil {
                        DispatchQueue.main.async {
                            ErrorHelper.setErrorLabel(defaultMessage: "Edit category failed, please try again later.", errorLabel: self.errorLabel, error: error)
                            self.editCategoryLoadingHelper.stopActivityIndicator(uiView: self.view)
                        }
                    }
                    else if success {
                        DispatchQueue.main.async {
                            self.editCategoryLoadingHelper.stopActivityIndicator(uiView: self.view)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                })
            }
        }
    }
    
    func executeAddCategory() {
        self.loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        //check if the category exists
        let existingCategories = PFQuery(className: "Category")
        //existingCategories.whereKey("name", equalTo: categoryNameTextField.text!)
        let canonicalName = categoryNameTextField.text?.lowercased()
        existingCategories.whereKey("canonicalName", equalTo: canonicalName!)
        existingCategories.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        
        existingCategories.findObjectsInBackground(block: { (objects, error) in
            //if it exists, alert user
            if error != nil {
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    ErrorHelper.setErrorLabel(defaultMessage: "Finding existing categories failed, please try again later.", errorLabel: self.errorLabel, error: error)
                }
            }
            else if (objects?.count)! > 0 {
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    self.errorLabel.text = "Category already exists!"
                }
            }
            else {
                //otherwise add the new category
                let newCategory = PFObject(className: "Category")
                
                newCategory["name"] = self.categoryNameTextField.text
                newCategory["owner"] = PFUser.current()?.objectId
                newCategory["canonicalName"] = self.categoryNameTextField.text?.lowercased()
                
                newCategory.saveInBackground(block: { (success, error) in
                    if success {
                        self.dismiss(animated: true, completion: nil)
                    }
                    else if error != nil {
                        DispatchQueue.main.async {
                            self.loadingHelper.stopActivityIndicator(uiView: self.view)
                            ErrorHelper.setErrorLabel(defaultMessage: "Add category failed, please try again later.", errorLabel: self.errorLabel, error: error)
                        }
                    }
                    DispatchQueue.main.async {
                        self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    }
                })
            }
        })
    }
    
    //# MARK: - IBAction Methods
    @IBAction func cancel(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addCategory(_ sender: AnyObject) {
        self.errorLabel.text = ""
        
        if categoryNameTextField.text == "" {
            errorLabel.text = "Category name is required"
        }
        else {
            if editMode {
                editCategory()
            }
            else {
                executeAddCategory()
            }
        }
    }
    
    //# MARK: - Touch Methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //# MARK: - TextField Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //# MARK: - View Methods
    override func viewDidAppear(_ animated: Bool) {
        BorderHelper.addBottomBorder(view: categoryNameTextField, color: UIColor.gray)
        
        if editMode {
            navigationBar.topItem?.title = "Edit Category"
            addCategoryButton.setTitle("Update Category", for: UIControlState.normal)
            
            categoryNameTextField.text = category["name"] as! String?
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
