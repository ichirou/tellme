//
//  AddTransactionViewController.swift
//  TellMe
//
//  Created by Richmond Ko on 28/09/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Parse

class AddTransactionViewController: UIViewController, UITextFieldDelegate, PassDataBackDelegate {
    
    var transaction = PFObject(className: "Expense")
    var category = PFObject(className: "Category")
    var account = PFObject(className: "Account")
    var selectedDate = Date()
    var loadingHelper = LoadingHelper()
    var editTransactionLoadingHelper = LoadingHelper()
    var selectedAccount = "Select Account"
    var selectedCategory = "Select Category"
    var selectedAccountRow = -1
    var selectedCategoryRow = -1
    var editMode = false
    var speechRecognitionMode = false
    var speechRecognitionData = [String: String]()
    
    @IBOutlet var navigationBar: UINavigationBar!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var dateTextField: UITextField!
    @IBOutlet var amountTextField: UITextField!
    @IBOutlet var descriptionTextField: UITextField!
    @IBOutlet var selectCategoryButton: UIButton!
    @IBOutlet var selectAccountButton: UIButton!
    @IBOutlet var transactionButton: UIButton!
    
    //# MARK: - Custom Methods
    func checkExistingAccounts() {
        self.loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        let existingAccounts = PFQuery(className: "Account")
        existingAccounts.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        existingAccounts.whereKey("name", equalTo: speechRecognitionData["account"]!)
        existingAccounts.findObjectsInBackground(block: { (objects, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    ErrorHelper.setErrorLabel(defaultMessage: "Searching existing accounts failed, please try again later.", errorLabel: self.errorLabel, error: error)
                }
                
            }
            else if (objects?.count)! > 0 {
                if let existingAccount = objects?[0] {
                    self.account = existingAccount
                    //self.checkAndAddNewCategoryThenExecuteAddTransaction()
                }
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                }
            }
            else if (objects?.count)! == 0 {
                if self.speechRecognitionData["account"] != "" {
                    let newAccount = PFObject(className: "Account")
                    newAccount["owner"] = PFUser.current()?.objectId
                    newAccount["name"] = self.speechRecognitionData["account"]
                    newAccount["type"] = "Cash"
                    
                    newAccount.saveInBackground(block: { (success, error) in
                        if error != nil {
                            DispatchQueue.main.async {
                                self.loadingHelper.stopActivityIndicator(uiView: self.view)
                                ErrorHelper.setErrorLabel(defaultMessage: "Add account failed, please try again later.", errorLabel: self.errorLabel, error: error)
                            }
                        }
                        else if success {
                            print("new account")
                            let query = PFQuery(className: "Account")
                            query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
                            query.whereKey("name", equalTo: self.speechRecognitionData["account"]!)
                            query.getFirstObjectInBackground(block: { (object, error) in
                                if error != nil {
                                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                                    ErrorHelper.setErrorLabel(defaultMessage: "Getting account, please try again later.", errorLabel: self.errorLabel, error: error)
                                }
                                else if object != nil {
                                    self.account = object!
                                    DispatchQueue.main.async {
                                        self.loadingHelper.stopActivityIndicator(uiView: self.view)
                                    }
                                    //self.checkAndAddNewCategoryThenExecuteAddTransaction()
                                }
                            })
                        }
                    })
                }
                else {
                    DispatchQueue.main.async {
                        self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    }
                }
            }
        })
    }
    
    func checkExistingCategories() {
        self.loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        let existingCategories = PFQuery(className: "Category")
        existingCategories.whereKey("canonicalName", equalTo: (speechRecognitionData["category"]?.lowercased())!)
        existingCategories.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        
        existingCategories.findObjectsInBackground(block: { (objects, error) in
            //if it exists, alert user
            if error != nil {
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    ErrorHelper.setErrorLabel(defaultMessage: "Finding existing categories failed, please try again later.", errorLabel: self.errorLabel, error: error)
                }
            }
            else if (objects?.count)! > 0 {
                if let existingCategory = objects?[0] {
                    self.category = existingCategory
                    //self.executeAddTransaction()
                }
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                }
            }
            else if (objects?.count)! == 0 {
                //add the new category if there is no existing category
                if self.speechRecognitionData["category"] != "" {
                    let newCategory = PFObject(className: "Category")
                    
                    newCategory["name"] = self.speechRecognitionData["category"]
                    newCategory["owner"] = PFUser.current()?.objectId
                    newCategory["canonicalName"] = self.speechRecognitionData["category"]?.lowercased()
                    
                    newCategory.saveInBackground(block: { (success, error) in
                        if error != nil {
                            DispatchQueue.main.async {
                                self.loadingHelper.stopActivityIndicator(uiView: self.view)
                                ErrorHelper.setErrorLabel(defaultMessage: "Add category failed, please try again later.", errorLabel: self.errorLabel, error: error)
                            }
                        }
                        else if success {
                            print("new category")
                            let query = PFQuery(className: "Category")
                            query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
                            query.whereKey("canonicalName", equalTo: (self.speechRecognitionData["category"]?.lowercased())!)
                            query.getFirstObjectInBackground(block: { (object, error) in
                                if error != nil {
                                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                                    ErrorHelper.setErrorLabel(defaultMessage: "Getting category, please try again later.", errorLabel: self.errorLabel, error: error)
                                }
                                else if object != nil {
                                    self.category = object!
                                    DispatchQueue.main.async {
                                        self.loadingHelper.stopActivityIndicator(uiView: self.view)
                                    }
                                    //self.executeAddTransaction()
                                }
                            })
                        }
                    })
                }
                else {
                    DispatchQueue.main.async {
                        self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    }
                }
            }
        })
    }
    
    func editTransaction() {
        editTransactionLoadingHelper.showActivityIndicatory(uiView: self.view, message: "Updating...")

        let query = PFQuery(className: "Expense")
        query.getObjectInBackground(withId: transaction.objectId!) { (object: PFObject?, error) in
            if error != nil {
                DispatchQueue.main.async {
                    ErrorHelper.setErrorLabel(defaultMessage: "Edit transaction failed, please try again later.", errorLabel: self.errorLabel, error: error)
                    self.editTransactionLoadingHelper.stopActivityIndicator(uiView: self.view)
                }
            }
            else if let transaction = object {
                let calendarcomponents = Calendar.current.dateComponents([.day, .month, .year], from: self.selectedDate)
                let month = calendarcomponents.month!
                let year = calendarcomponents.year!
                
                transaction["description"] = self.descriptionTextField.text
                transaction["amount"] = Double(self.amountTextField.text!)!
                transaction["date"] = self.selectedDate
                transaction["canonicalDescription"] = self.descriptionTextField.text?.lowercased()
                transaction["month"] = month
                transaction["year"] = year
                let categoryPointer = PFObject(withoutDataWithClassName: "Category", objectId: self.category.objectId)
                transaction["category_pointer"] = categoryPointer
                let accountPointer = PFObject(withoutDataWithClassName: "Account", objectId: self.account.objectId)
                transaction["account_pointer"] = accountPointer
                
                transaction.saveInBackground(block: { (success, error) in
                    if error != nil {
                        DispatchQueue.main.async {
                            ErrorHelper.setErrorLabel(defaultMessage: "Edit transaction failed, please try again later.", errorLabel: self.errorLabel, error: error)
                            self.editTransactionLoadingHelper.stopActivityIndicator(uiView: self.view)
                        }

                    }
                    else if success {
                        DispatchQueue.main.async {
                            self.editTransactionLoadingHelper.stopActivityIndicator(uiView: self.view)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                })
            }
        }
    }
    
    func executeAddTransaction() {
        self.loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        
        let transaction = PFObject(className: "Expense")
        let calendarcomponents = Calendar.current.dateComponents([.day, .month, .year], from: selectedDate)
        let month = calendarcomponents.month!
        let year = calendarcomponents.year!
        let categoryPointer = PFObject(withoutDataWithClassName: "Category", objectId: self.category.objectId)
        let accountPointer = PFObject(withoutDataWithClassName: "Account", objectId: self.account.objectId)
        transaction["owner"] = PFUser.current()?.objectId
        transaction["description"] = descriptionTextField.text
        
        transaction["amount"] = Double(amountTextField.text!)!
        transaction["date"] = selectedDate
        transaction["canonicalDescription"] = self.descriptionTextField.text?.lowercased()
        transaction["month"] = month
        transaction["year"] = year
        transaction["category_pointer"] = categoryPointer
        transaction["account_pointer"] = accountPointer
        
        transaction.saveInBackground { (success, error) in
            if success {
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    self.dismiss(animated: true, completion: nil)
                }
            }
            else if error != nil {
                DispatchQueue.main.async {
                    ErrorHelper.setErrorLabel(defaultMessage: "Add transaction failed, please try again later.", errorLabel: self.errorLabel, error: error)

                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                }
            }
        }
    }
    
    //# MARK: - IBAction Methods
    @IBAction func micButton(_ sender: AnyObject) {
        performSegue(withIdentifier: "showSpeechRecognition", sender: self)
    }
    
    @IBAction func cancel(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTransaction(_ sender: AnyObject) {
        errorLabel.text = ""
        //checking of input fields
        if descriptionTextField.text == "" {
            self.errorLabel.text = "Description is required."
        }
        else if amountTextField.text == "" {
            self.errorLabel.text = "Amount is required."
        }
        else if selectAccountButton.titleLabel?.text == "Select Account" {
            self.errorLabel.text = "Please select an account"
        }
        else if selectCategoryButton.titleLabel?.text == "Select Category" {
            self.errorLabel.text = "Please select a category"
        }
        else {
            if editMode {
                editTransaction()
            }
            else {
                executeAddTransaction()
            }
        }
    }
    
    //# MARK: - DatePicker Methods
    func datePickerChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        dateTextField.text = formatter.string(from: sender.date)
        selectedDate = sender.date
    }
    
    //# MARK: - TextField Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 0 {
            let datePicker = UIDatePicker()
            datePicker.datePickerMode = .date
            textField.inputView = datePicker
            let formatter = DateFormatter()
            formatter.dateStyle = .full
            dateTextField.text = formatter.string(from: selectedDate)
            datePicker.addTarget(self, action: #selector(self.datePickerChanged(sender:)), for: .valueChanged)
        }
    }
    
    //limit amount input to valid decimal numbers
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch string {
            case "0","1","2","3","4","5","6","7","8","9":
                return true
            case ".":
                let array = Array((textField.text?.characters)!)
                var decimalCount = 0
                for character in array {
                    if character == "." {
                        decimalCount += 1
                    }
                }
                
                if decimalCount == 1 {
                    return false
                } else {
                    return true
                }
            default:
                let array = Array(string.characters)
                if array.count == 0 {
                    return true
                }
                return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    //# MARK: - View Methods
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateTextField.delegate = self
        amountTextField.delegate = self
        
        if editMode {
            //if edit mode, get transaction information and display it
            let formatter = DateFormatter()
            formatter.dateStyle = .full
            
            let dateText = formatter.string(from: transaction["date"] as! Date)
            
            //set navigation bar title and button title
            navigationBar.topItem?.title = "Edit Transaction"
            transactionButton.setTitle("Update Transaction", for: UIControlState.normal)
            
            descriptionTextField.text = transaction["description"] as! String?
            amountTextField.text = String(format:"%.2f", abs(transaction["amount"] as! Double))
            account = transaction["account_pointer"] as! PFObject
            selectAccountButton.setTitle(account["name"] as! String?, for: UIControlState.normal)
            category = transaction["category_pointer"] as! PFObject
            selectCategoryButton.setTitle(category["name"] as! String?, for: UIControlState.normal)
            dateTextField.text = dateText
            selectedDate = transaction["date"] as! Date
            
            selectCategoryButton.setTitleColor(UIColor.black, for: UIControlState.normal)
            selectAccountButton.setTitleColor(UIColor.black, for: UIControlState.normal)
        }
        
        var texts = [String]()
        texts.append("I spent 4999.12 for Single Origin under Food using my wallet")
        texts.append("I spent 4999.12 for Single Origin under Food using bank account")
        texts.append("I spent 4999.12 for Single Origin under Food using")
        texts.append("I spent 4999.12 for Single Origin under Food")
        texts.append("I paid 4999.12 for Single Origin under Food")
        texts.append("I paid 4999.12 for Single Origin under Food and restaurant")
        texts.append("paid 3112 for ipad air")
        texts.append("")
        texts.append("random gibberish that i said yesterday")
        texts.append("random gibberish that i said yesterday for under")
        texts.append("for random under gibberish that i said yesterday for under")
        texts.append("I paid 4999.12")
        texts.append("I paid for mcdonalds")
        texts.append("something")
        texts.append("spent")
        texts.append("paid for")
        texts.append("I spend 500 for shoes underclothes using my alarm")
        texts.append("I spent 100 for san pellegrino using my wallet under food")
        
        for text in texts {
            print(SpeechRecognitionHelper.recognizeDataFromText(text: text))
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        BorderHelper.addBottomBorder(view: descriptionTextField, color: UIColor.gray)
        BorderHelper.addBottomBorder(view: amountTextField, color: UIColor.gray)
        BorderHelper.addBottomBorder(view: selectCategoryButton, color: UIColor.gray)
        BorderHelper.addBottomBorder(view: selectAccountButton, color: UIColor.gray)
        BorderHelper.addBottomBorder(view: dateTextField, color: UIColor.gray)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //# MARK: - Custom Delegate Methods
    func setSelectedAccountRow(value: Int) {
        selectedAccountRow = value
    }
    
    func setSelectedCategoryRow(value: Int) {
        selectedCategoryRow = value
    }
    
    func setSelectedAccountButtonTitle(value: String) {
        selectAccountButton.setTitle(value, for: UIControlState.normal)
    }
    
    func setSelectedCategoryButtonTitle(value: String) {
        selectCategoryButton.setTitle(value, for: UIControlState.normal)
    }
    
    func setSelectAccountButtonTextColor(color: UIColor) {
        selectAccountButton.setTitleColor(color, for: UIControlState.normal)
    }
    
    func setSelectCategoryButtonTextColor(color: UIColor) {
        selectCategoryButton.setTitleColor(color, for: UIControlState.normal)
    }
    
    func setSpeechRecognitionData(data: [String : String]) {
        speechRecognitionMode = true
        speechRecognitionData = data
        descriptionTextField.text = data["description"]
        amountTextField.text = data["amount"]
        
        if data["category"] == "" {
            selectCategoryButton.setTitle("Select Category", for: UIControlState.normal)
        }
        else {
            selectCategoryButton.setTitle(data["category"], for: UIControlState.normal)
            selectCategoryButton.setTitleColor(UIColor.black, for: UIControlState.normal)
        }
        
        if data["account"] == "" {
            selectAccountButton.setTitle("Select Account", for: UIControlState.normal)
        }
        else {
            selectAccountButton.setTitle(data["account"], for: UIControlState.normal)
            selectAccountButton.setTitleColor(UIColor.black, for: UIControlState.normal)
        }
        
    }
    
    func setSelectedCategoryObject(category: PFObject) {
        self.category = category
    }
    
    func setSelectedAccountObject(account: PFObject) {
        self.account = account
    }
    
    func checkExistingCategoriesAndAccounts() {
        checkExistingAccounts()
        checkExistingCategories()
    }
    
    //# MARK: - Segue Methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSelectAccount" {
            let destinationViewController = segue.destination as! AccountsTableViewController
            destinationViewController.delegate = self
            destinationViewController.selectedAccountRow = selectedAccountRow
            destinationViewController.sourceViewController = "addTransaction"
        }
        else if segue.identifier == "showSelectCategory" {
            let destinationViewController = segue.destination as! CategoriesTableViewController
            destinationViewController.delegate = self
            destinationViewController.selectedCategoryRow = selectedCategoryRow
            destinationViewController.sourceViewController = "addTransaction"
        }
        else if segue.identifier == "showSpeechRecognition" {
            let destinationViewController = segue.destination as! SpeechRecognitionViewController
            destinationViewController.delegate = self
        }
    }
}
