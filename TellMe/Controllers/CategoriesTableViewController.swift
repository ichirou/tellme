//
//  CategoriesTableViewController.swift
//  TellMe
//
//  Created by Richmond Ko on 02/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Parse

class CategoriesTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    var delegate: PassDataBackDelegate?
    var dashboardDelegate: DashboardDelegate?
    var categories = [PFObject]()
    var loadingHelper = LoadingHelper()
    var searchLoadingHelper = LoadingHelper()
    var selectedCategory = String()
    var selectedCategoryRow = -1
    var sourceViewController = String()
    var selectedRows = [Int]()
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet var navigationBar: UINavigationBar!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var editButton: UIBarButtonItem!
    @IBOutlet var deleteButton: UIBarButtonItem!
    
    //# MARK: - Custom Methods
    func searchCategories(name: String) {
        tableView.isScrollEnabled = true
        searchLoadingHelper.showLoadingWithoutIgnoringInteractionEvents(uiView: self.view, message: "Searching...")
        
        let query = PFQuery(className: "Category")
        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        query.whereKey("canonicalName", contains: name.lowercased())
        
        //find objects in background
        query.findObjectsInBackground { (objects, error) in
            //if there is an error
            if error != nil {
                DispatchQueue.main.async {
                    self.searchLoadingHelper.stopLoading(uiView: self.view)
                    let errorMessage = ErrorHelper.parseError(defaultMessage: "Searching categories failed, please try again later.", error: error)
                    
                    AlertHelper.createAlert(title: "Error", message: errorMessage, view: self)
                }
            }
            else {
                //clear all storage arrays for fresh data
                self.categories.removeAll()
                
                if let categories = objects {
                    for category in categories {
                        self.categories.append(category)
                    }
                }
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.searchLoadingHelper.stopLoading(uiView: self.view)
            }
        }
    }
    
    func getCategories() {
        tableView.isScrollEnabled = true
        loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        let categories = PFQuery(className: "Category")
        
        categories.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        categories.order(byAscending: "name")
        
        categories.findObjectsInBackground { (objects, error) in
            if error != nil {
                DispatchQueue.main.async {
                    let errorMessage = ErrorHelper.parseError(defaultMessage: "Finding categories failed, please try again later.", error: error)
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    AlertHelper.createAlert(title: "Error", message: errorMessage, view: self)
                }
            }
            else {
                self.categories.removeAll()
                if let categories = objects {
                    for category in categories {
                        self.categories.append(category)
                    }
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                }
            }
        }
    }
    
    //# MARK: - IBAction Methods
    @IBAction func cancel(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func editCategory(_ sender: AnyObject) {
        performSegue(withIdentifier: "showEditCategory", sender: self)
    }
    
    @IBAction func deleteCategory(_ sender: AnyObject) {
        //delete multiple categories now supported
        var objectIDsToDelete = [String]()
        var categoriesToDelete = [PFObject]()
        
        for selectedRow in selectedRows {
            objectIDsToDelete.append(categories[selectedRow].objectId!)
            categoriesToDelete.append(categories[selectedRow])
        }
        
        let query = PFQuery(className: "Expense")
        query.whereKey("category_pointer", containedIn: categoriesToDelete)
        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        
        query.findObjectsInBackground(block: { (objects, error) in
            if error != nil {
                DispatchQueue.main.async {
                    let errorMessage = ErrorHelper.parseError(defaultMessage: "Finding expense with existing categories failed, please try again later.", error: error)
                    AlertHelper.createAlert(title: "Error", message: errorMessage, view: self)
                }
            }
            else if (objects?.count)! > 0 {
                AlertHelper.createAlert(title: "Cannot delete category", message: "One or more of the selected categories are currently being used, edit or delete first the related transactions", view: self)
            }
            else {
                AlertHelper.createMultipleDeleteAlert(title: "Delete Confirmation", message: "Are you sure you want to delete these categories?", view: self, tableView: self.tableView, funcIfOK: ParseOperations.deleteObjectsWith, className: "Category", objectIds: objectIDsToDelete, funcToExecuteAfterDelete: self.getCategories, uiViewController: self)
            }
            self.selectedRows.removeAll()
            self.editButton.isEnabled = false
            self.deleteButton.isEnabled = false
            self.tableView.isScrollEnabled = true
            self.tableView.reloadData()
        })
    }
    
    //# MARK: - View Methods
    override func viewDidAppear(_ animated: Bool) {
        getCategories()
        selectedRows.removeAll()
        editButton.isEnabled = false
        deleteButton.isEnabled = false
        if sourceViewController == "menu" {
            navigationBar.topItem?.title = "Manage Categories"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        tableView.tableFooterView = UIView()
    }

    override func viewDidDisappear(_ animated: Bool) {
        if sourceViewController == "menu" {
            dashboardDelegate?.reloadDashboard()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //# MARK: - SearchBar Methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchCategories(name: searchText)
        editButton.isEnabled = false
        deleteButton.isEnabled = false
        selectedRows.removeAll()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        getCategories()
        view.endEditing(true)
    }
    
    //search function
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        searchCategories(name: searchBar.text!)
    }

    //# MARK: - TableView Methods
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }

    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        cell.textLabel?.text = categories[indexPath.row]["name"] as! String?

        if sourceViewController == "addTransaction" {
            if indexPath.row == selectedCategoryRow {
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
        }
        else if sourceViewController == "menu" {
            cell.accessoryType = .none
            cell.selectionStyle = .none
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        
        if sourceViewController == "addTransaction" {
            selectedCategory = (categories[indexPath.row]["name"] as! String?)!
            
            delegate?.setSelectedCategoryObject(category: categories[indexPath.row])
            delegate?.setSelectedCategoryButtonTitle(value: selectedCategory)
            delegate?.setSelectedCategoryRow(value: indexPath.row)
            delegate?.setSelectCategoryButtonTextColor(color: UIColor.black)
            self.dismiss(animated: true, completion: nil)

        }
        else if sourceViewController == "menu" {
            
            if cell?.accessoryType == UITableViewCellAccessoryType.checkmark {
                
                cell?.accessoryType = .none
                selectedRows.removeObject(object: indexPath.row)
                
                if selectedRows.count == 1 {
                    editButton.isEnabled = true
                }
                else if selectedRows.count == 0 {
                    deleteButton.isEnabled = false
                    editButton.isEnabled = false
                    tableView.isScrollEnabled = true
                }
                print(selectedRows)
            }
            else if cell?.accessoryType == UITableViewCellAccessoryType.none {
                
                cell?.accessoryType = .checkmark
                selectedRows.append(indexPath.row)
                tableView.isScrollEnabled = false
                if selectedRows.count == 1 {
                    editButton.isEnabled = true
                    deleteButton.isEnabled = true
                }
                else if selectedRows.count > 1 {
                    editButton.isEnabled = false
                }
                print(selectedRows)
            }

        }
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
    
    internal func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if sourceViewController == "menu" {
            return true
        }
        else {
            return false
        }
    }
    
    //# MARK: - Segue Methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEditCategory" {
            let destinationController = segue.destination as! AddCategoryViewController
            destinationController.editMode = true
            destinationController.category = categories[selectedRows[0]]
        }
    }
}
