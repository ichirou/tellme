//
//  ExpenseForCategoryViewController.swift
//  TellMe
//
//  Created by Richmond Ko on 12/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Parse

class ExpenseForCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var expenses = [PFObject]()
    var loadingHelper = LoadingHelper()
    var searchLoadingHelper = LoadingHelper()
    var fromIndex = 0
    var noMoreTransactions = false
    var selectedCategory = String()
    var selectedCategoryObject = PFObject(className: "Category")
    var selectedMonth = 0
    var selectedYear = 0
    var selectedRows = [Int]()
    var isYearlyMode = false
    
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var editButton: UIBarButtonItem!
    @IBOutlet var deleteButton: UIBarButtonItem!
    @IBOutlet var navigationBar: UINavigationBar!
    
    //# MARK: - Custom Methods
    
    func searchTransaction(description: String) {
        searchLoadingHelper.showLoadingWithoutIgnoringInteractionEvents(uiView: self.view, message: "Searching...")
        //create expense query to find expenses/transaction
        let expenseQuery = PFQuery(className: "Expense")
        expenseQuery.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        expenseQuery.whereKey("canonicalDescription", contains: description.lowercased())
        expenseQuery.whereKey("category_pointer", equalTo: selectedCategoryObject)
        expenseQuery.whereKey("year", equalTo: selectedYear)
        expenseQuery.includeKey("category_pointer")
        expenseQuery.includeKey("account_pointer")
        if !isYearlyMode {
            expenseQuery.whereKey("month", equalTo: selectedMonth)
        }
        expenseQuery.order(byDescending: "date")
        
        //find objects in background
        expenseQuery.findObjectsInBackground { (objects, error) in
            //if there is an error
            if error != nil {
                DispatchQueue.main.async {
                    let errorMessage = ErrorHelper.parseError(defaultMessage: "Searching expenses failed, please try again later.", error: error)
                    
                    AlertHelper.createAlert(title: "Error", message: errorMessage, view: self)
                }
            }
            else {
                //clear all storage arrays for fresh data
                self.expenses.removeAll()
                
                if let expenses = objects {
                    for expense in expenses {
                        self.expenses.append(expense)
                    }
                }
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableView.isScrollEnabled = true
                self.searchLoadingHelper.stopLoading(uiView: self.view)
            }
        }
    }
    
    func reloadTransactionsFromStart() {
        //load transactions from scratch
        fromIndex = 0
        expenses.removeAll()
        noMoreTransactions = false
        loadTransactions()
    }
    
    func loadTransactions() {
        //hide tool bar when loading transactions
        loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        
        //create expense query to find expenses/transaction
        let expenseQuery = PFQuery(className: "Expense")
        expenseQuery.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        expenseQuery.whereKey("category_pointer", equalTo: selectedCategoryObject)
        expenseQuery.whereKey("year", equalTo: selectedYear)
        expenseQuery.includeKey("category_pointer")
        expenseQuery.includeKey("account_pointer")
        if !isYearlyMode {
            expenseQuery.whereKey("month", equalTo: selectedMonth)
        }
        expenseQuery.order(byDescending: "date")
        //limit to 5 items only and skip fromIndex
        expenseQuery.limit = 5
        expenseQuery.skip = fromIndex
        
        //find objects in background
        expenseQuery.findObjectsInBackground { (objects, error) in
            //if there is an error
            if error != nil {
                DispatchQueue.main.async {
                    let errorMessage = ErrorHelper.parseError(defaultMessage: "Loading expenses failed, please try again later.", error: error)
                    
                    AlertHelper.createAlert(title: "Error", message: errorMessage, view: self)
                }
            }
            else {
                if let expenses = objects {
                    //if no more objects
                    if expenses.count == 0 {
                        self.noMoreTransactions = true
                    }
                    else {
                        //store how many objects I got for later skipping
                        self.fromIndex += expenses.count
                        for expense in expenses {
                            self.expenses.append(expense)
                        }
                    }
                }
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.loadingHelper.stopActivityIndicator(uiView: self.view)
            }
        }
    }
    
    //# MARK: - IBAction Methods
    @IBAction func cancel(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func editTrasaction(_ sender: AnyObject) {
        performSegue(withIdentifier: "showManageTransaction", sender: self)
    }
    
    @IBAction func deleteTransaction(_ sender: AnyObject) {
        var objectIDsToDelete = [String]()
        
        for selectedRow in selectedRows {
            objectIDsToDelete.append(expenses[selectedRow].objectId!)
        }
        
        AlertHelper.createMultipleDeleteAlert(title: "Delete Confirmation", message: "Are you sure you want to delete these transactions?", view: self, tableView: self.tableView, funcIfOK: ParseOperations.deleteObjectsWith, className: "Expense", objectIds: objectIDsToDelete, funcToExecuteAfterDelete: reloadTransactionsFromStart, uiViewController: self)
        
        selectedRows.removeAll()
        editButton.isEnabled = false
        deleteButton.isEnabled = false
    }
    
    //# MARK: - TableView Methods
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if expenses.count == 0 {
            return 1
        }
        else {
            return expenses.count
        }
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let prototypeCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ExpenseTableViewCell
        
        if expenses.count == 0 {
            prototypeCell.textLabel?.numberOfLines = 0
            prototypeCell.textLabel?.text = "Nothing to show."
            prototypeCell.expenseLabel.text = ""
            prototypeCell.accountLabel.text = ""
            prototypeCell.categoryLabel.text = ""
            prototypeCell.amountLabel.text = ""
            prototypeCell.dateLabel.text = ""
            prototypeCell.accessoryType = .none
            prototypeCell.accountIcon.isHidden = true
            prototypeCell.categoryIcon.isHidden = true
        }
        else {
            let formatter = DateFormatter()
            let numberFormatter = NumberFormatter()
            
            formatter.dateFormat = "E, MMM d, yyyy"
            
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Locale.current
            
            let amountText = numberFormatter.string(from: expenses[indexPath.row]["amount"] as! NSNumber)
            
            let dateText = formatter.string(from: expenses[indexPath.row]["date"] as! Date)
            
            prototypeCell.textLabel?.text = ""
            prototypeCell.expenseLabel.text = expenses[indexPath.row]["description"] as! String?
            let accountObject = expenses[indexPath.row]["account_pointer"] as! PFObject
            prototypeCell.accountLabel.text = accountObject["name"] as! String?
            let categoryObject = expenses[indexPath.row]["category_pointer"] as! PFObject
            prototypeCell.categoryLabel.text = categoryObject["name"] as! String?
            prototypeCell.amountLabel.text = amountText
            prototypeCell.dateLabel.text = dateText
            prototypeCell.accessoryType = .none
            prototypeCell.selectionStyle = .none
            prototypeCell.accountIcon.isHidden = false
            prototypeCell.categoryIcon.isHidden = false
            
            //if last row
            if indexPath.row == expenses.count - 1 && noMoreTransactions == false {
                loadTransactions()
            }
        }
        
        return prototypeCell
    }
    
    internal func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if expenses.count == 0 {
            return false
        }
        else {
            return true
        }
    }
    
    internal func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            self.deleteTransaction(self)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //cell selected
        if expenses.count != 0 {
            let cell = tableView.cellForRow(at: indexPath)
            if cell?.accessoryType == UITableViewCellAccessoryType.checkmark {
                
                cell?.accessoryType = .none
                selectedRows.removeObject(object: indexPath.row)
                
                if selectedRows.count == 1 {
                    editButton.isEnabled = true
                }
                else if selectedRows.count == 0 {
                    deleteButton.isEnabled = false
                    editButton.isEnabled = false
                    tableView.isScrollEnabled = true
                }
                print(selectedRows)
            }
            else if cell?.accessoryType == UITableViewCellAccessoryType.none {
                
                cell?.accessoryType = .checkmark
                selectedRows.append(indexPath.row)
                tableView.isScrollEnabled = false
                if selectedRows.count == 1 {
                    editButton.isEnabled = true
                    deleteButton.isEnabled = true
                }
                else if selectedRows.count > 1 {
                    editButton.isEnabled = false
                }
                print(selectedRows)
            }
        }
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //# MARK: - SearchBar Methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchTransaction(description: searchText)
        editButton.isEnabled = false
        deleteButton.isEnabled = false
        selectedRows.removeAll()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        reloadTransactionsFromStart()
        view.endEditing(true)
    }
    
    //search function
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        searchTransaction(description: searchBar.text!)
    }
    
    //# MARK: - View Methods
    override func viewDidAppear(_ animated: Bool) {
        //find all expense owned by current logged in user
        reloadTransactionsFromStart()
        selectedRows.removeAll()
        editButton.isEnabled = false
        deleteButton.isEnabled = false
        (UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self])).tintColor = UIColor.white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        tableView.tableFooterView = UIView()
        
        navigationBar.topItem?.title = selectedCategory
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //# MARK: - Segue Methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showManageTransaction" {
            let destinationViewController = segue.destination as! AddTransactionViewController
            destinationViewController.editMode = true
            destinationViewController.transaction = expenses[selectedRows[0]]
        }
    }

}
