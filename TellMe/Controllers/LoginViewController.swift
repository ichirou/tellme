//
//  LoginViewController.swift
//  TellMe
//
//  Created by Richmond Ko on 28/09/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Parse
import FBSDKLoginKit
import ParseFacebookUtilsV4

class LoginViewController: UIViewController {

    var loadingHelper = LoadingHelper()
    var loginMode = true
    
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var loginOrSignUpButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var facebookLoginButton: UIButton!
    @IBOutlet var alphaBackground: UIView!
    
    @IBAction func loginUsingFacebook(_ sender: AnyObject) {
        let permissions = ["public_profile","email"]
        
        PFFacebookUtils.logInInBackground(withReadPermissions: permissions) { (user, error) in
            if let user = user {
                if user.isNew {
                    print("User signed up and logged in through Facebook!")
                    let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email, name"])
                    let connection = FBSDKGraphRequestConnection()
                    connection.add(graphRequest, completionHandler: { (connection, result, error) in
                        if error != nil {
                            print(error!)
                        }
                        else {
                            if let userDetails = result as? [String: String] {
                                user.email = userDetails["email"]
                                user.saveEventually()
                            }
                        }
                    })
                    connection.start()
                    self.performSegue(withIdentifier: "goToDashboard", sender: self)
                } else {
                    print("User logged in through Facebook!")
                    self.performSegue(withIdentifier: "goToDashboard", sender: self)
                }
            } else {
                print("Uh oh. The user cancelled the Facebook login.")
            }
        }
    }
    
    //# MARK: - IBAction Methods
    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {}
    
    @IBAction func loginOrSignUpChange(_ sender: AnyObject) {
        
        if loginMode {
            errorLabel.text = ""
            loginMode = false
            emailTextField.placeholder = "Enter your email address"
            passwordTextField.placeholder = "Create a password"
            loginButton.setTitle("Sign Up", for: UIControlState.normal)
            loginOrSignUpButton.setTitle("Log In", for: UIControlState.normal)
        }
        else {
            errorLabel.text = ""
            loginMode = true
            emailTextField.placeholder = "Email"
            passwordTextField.placeholder = "Password"
            loginButton.setTitle("Log In", for: UIControlState.normal)
            loginOrSignUpButton.setTitle("Sign Up", for: UIControlState.normal)
        }
    }

    //login or create new account
    @IBAction func loginOrCreateAccount(_ sender: AnyObject) {
        errorLabel.text = ""
        loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        //login mode
        if loginMode {
            
            PFUser.logInWithUsername(inBackground: emailTextField.text!, password: passwordTextField.text!, block: { (user, error) in
                
                if error != nil {
                    let errorString = ErrorHelper.parseError(defaultMessage: "Login failed, please try again later.", error: error)
                    AlertHelper.createAlert(title: "Error", message: errorString, view: self)
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                }
                else {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    self.performSegue(withIdentifier: "goToDashboard", sender: self)
                }
            })
        }
        //create account mode
        else {
            let newUser = PFUser()
            
            //input checkers
            if emailTextField.text == "" || passwordTextField.text == "" {
                AlertHelper.createAlert(title: "Error", message: "Email and password is required", view: self)
                loadingHelper.stopActivityIndicator(uiView: self.view)
            }
            else if passwordTextField.text!.characters.count < 8 {
                AlertHelper.createAlert(title: "Error", message: "Password must be 8 or more characters", view: self)
                loadingHelper.stopActivityIndicator(uiView: self.view)

            }
            else {
                //access control list
                let newUserACL = PFACL()
                newUserACL.getPublicReadAccess = true
                
                newUser.acl = newUserACL
                newUser.username = emailTextField.text
                newUser.password = passwordTextField.text
                newUser.email = emailTextField.text
                
                //new user signup
                newUser.signUpInBackground(block: { (success, error) in
                    if success {
                        self.loadingHelper.stopActivityIndicator(uiView: self.view)
                        self.dismiss(animated: false, completion: nil)
                        AlertHelper.createAlert(title: "Sign up success", message: "Please log in", view: self)
                    }
                    else if error != nil {
                        let errorString = ErrorHelper.parseError(defaultMessage: "Create account failed, please try again later.", error: error)
                        AlertHelper.createAlert(title: "Error", message: errorString, view: self)

                        self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    }
                    
                })// end of new user sign up
            }
        }
    }
    
    //# MARK: - Touch Methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //# MARK: - TextField Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    //# MARK: - View Methods
    override func viewDidAppear(_ animated: Bool) {
        if PFUser.current() != nil {
            PFUser.logOut()
        }
        view.sendSubview(toBack: alphaBackground)
        //emailTextField.becomeFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "penguinBlue.jpg")!)
    }

    override func viewDidLayoutSubviews() {
        BorderHelper.addBottomBorder(view: emailTextField, color: UIColor.white)
        BorderHelper.addBottomBorder(view: passwordTextField, color: UIColor.white)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
