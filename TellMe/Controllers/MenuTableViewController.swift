//
//  MenuTableViewController.swift
//  TellMe
//
//  Created by Richmond Ko on 01/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Parse

class MenuTableViewController: UITableViewController {

    var dashboardDelegate: DashboardDelegate?
    var menuItems = ["Hello, " + (PFUser.current()?.email)!, "Manage Accounts", "Manage Categories", "Monthly Reports", "Yearly Reports", "Logout"]
    var loadingHelper = LoadingHelper()
    
    //# MARK: - TableView Methods
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  menuItems.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.textLabel?.text = menuItems[indexPath.row]
        
        if indexPath.row == 0 {
            cell.textLabel?.textColor = UIColor.black
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15.0)
        }
        else {
            cell.textLabel?.textColor = UIColor.darkGray
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: 0.1)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)! as UITableViewCell

        if cell.textLabel?.text == "Logout" {
            self.loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
            
            PFUser.logOutInBackground { (error) in
                if error != nil {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    let errorMessage = ErrorHelper.parseError(defaultMessage: "Logging out failed, please try again later.", error: error)
                    
                    AlertHelper.createAlert(title: "Error", message: errorMessage, view: self)
                }
                else {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    //must unwind here
                    self.performSegue(withIdentifier: "unwindToLogin", sender: self)
                }
            }
        }
        else if cell.textLabel?.text == "Manage Accounts" {
            self.performSegue(withIdentifier: "showAccountsTable", sender: self)
        }
        else if cell.textLabel?.text == "Manage Categories" {
            self.performSegue(withIdentifier: "showCategoriesTable", sender: self)
        }
        else if cell.textLabel?.text == "Monthly Reports" {
            self.performSegue(withIdentifier: "showMonthlyReports", sender: self)
        }
        else if cell.textLabel?.text == "Yearly Reports" {
            self.performSegue(withIdentifier: "showYearlyReports", sender: self)
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //# MARK: - Segue Methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAccountsTable" {
            let destinationViewController = segue.destination as! AccountsTableViewController
            destinationViewController.sourceViewController = "menu"
            destinationViewController.dashboardDelegate = dashboardDelegate
        }
        else if segue.identifier == "showCategoriesTable" {
            let destinationViewController = segue.destination as! CategoriesTableViewController
            destinationViewController.sourceViewController = "menu"
            destinationViewController.dashboardDelegate = dashboardDelegate
        }
        else if segue.identifier == "showMonthlyReports" {
            let destinationViewController = segue.destination as! ReportsViewController
            destinationViewController.isYearlyMode = false
            destinationViewController.dashboardDelegate = dashboardDelegate
        }
        else if segue.identifier == "showYearlyReports" {
            let destinationViewController = segue.destination as! ReportsViewController
            destinationViewController.isYearlyMode = true
            destinationViewController.dashboardDelegate = dashboardDelegate
        }
    }
    
    //# MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
