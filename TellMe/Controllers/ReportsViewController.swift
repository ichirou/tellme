//
//  ReportsViewController.swift
//  TellMe
//
//  Created by Richmond Ko on 06/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Parse
import Charts
import RandomColorSwift

class ReportsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var dashboardDelegate: DashboardDelegate?
    var totalExpensesPerMonth = [Int: Double]()
    var categories = [PFObject] ()
    var accounts = [PFObject] ()
    var selectedCategory = String()
    var selectedCategoryObject = PFObject(className: "Category")
    var yearPickerData = [Int]()
    var totalExpense = 0.0
    var selectedMonth = 0
    var selectedYear = 0
    var isYearlyMode = false
    var gotTotalExpense = false
    var gotTotalExpenseByMonth = false
    var gotTotalExpenseByCategory = false
    var gotTotalExpenseByAccount = false
    
    let date = Date()
    let yearPicker = UIPickerView()
    let loadingHelper = LoadingHelper()
    let cellLoadingHelper = LoadingHelper()
    let currentCalendar = Calendar.current
    let gotTotalExpenseNotification = Notification.Name("gotTotalExpenseNotification")
    let gotTotalExpensePerMonthNotification = Notification.Name("gotTotalExpensePerMonthNotification")
    let gotTotalExpenseByCategoryNotification = Notification.Name("gotTotalExpenseByCategoryNotification")
    let gotTotalExpenseByAccountNotification = Notification.Name("gotTotalExpenseByAccountNotification")
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var navigationBar: UINavigationBar!
    
    //# MARK: - Custom Methods
    
    func reloadTableAndStopLoading() {
        self.tableView.reloadData()
//        var indexPaths = [IndexPath]()
//        for i in 0...self.tableView.numberOfRows(inSection: 0) - 1 {
//            let indexPath = IndexPath(row: i, section: 0)
//            indexPaths.append(indexPath)
//            self.tableView.cellForRow(at: indexPath)?.setNeedsDisplay()
//        }
//        self.tableView.reloadRows(at: indexPaths, with: UITableViewRowAnimation.middle)
        self.loadingHelper.stopActivityIndicator(uiView: self.view)
    }
    
    func catchReportsNotifications(notification: Notification) {
        if notification.name.rawValue == "gotTotalExpenseNotification" {
            print("gotTotalExpense")
            gotTotalExpense = true
        }
        if notification.name.rawValue == "gotTotalExpenseByCategoryNotification" {
            print("gotTotalExpenseByCategory")
            gotTotalExpenseByCategory = true
        }
        if notification.name.rawValue == "gotTotalExpenseByAccountNotification" {
            print("gotTotalExpenseByAccount")
            gotTotalExpenseByAccount = true
        }
        if notification.name.rawValue == "gotTotalExpensePerMonthNotification" {
            print("gotTotalExpensePerMonth")
            gotTotalExpenseByMonth = true
        }
        if !isYearlyMode && gotTotalExpense && gotTotalExpenseByCategory && gotTotalExpenseByAccount {
            gotTotalExpenseByAccount = false
            gotTotalExpenseByCategory = false
            gotTotalExpense = false
            print("monthly mode: got all notifications, reloading table and stopping spinner")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                self.reloadTableAndStopLoading()
            })
        }
        else if isYearlyMode && gotTotalExpense && gotTotalExpenseByCategory && gotTotalExpenseByMonth {
            gotTotalExpenseByMonth = false
            gotTotalExpenseByCategory = false
            gotTotalExpense = false
            print("yearly mode: got all notifications, reloading table and stopping spinner")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                self.reloadTableAndStopLoading()
            })
        }
    }

    func generateMonthlyReports(forMonth: Int, forYear: Int) {
        loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        getTotalExpenses(forMonth: forMonth, forYear: forYear)
        setTotalExpensesByCategory(forMonth: forMonth, forYear: forYear)
        setTotalExpenseByAccount(forMonth: forMonth, forYear: forYear)
    }
    
    func generateYearlyReports(forYear: Int) {
        loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        getTotalExpenses(forYear: forYear)
        setTotalExpensesByCategory(forYear: forYear)
        getTotalMonthlyExpenses(forYear: forYear)
    }
    
    //gets total expenses per account for a certain month and year
    func setTotalExpenseByAccount(forMonth: Int, forYear: Int) {
        let query = PFQuery(className: "Account")
        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        query.order(byAscending: "name")
        
        query.findObjectsInBackground { (objects, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    let parsedError = ErrorHelper.parseError(defaultMessage: "Getting accounts failed. Please try again later.", error: error)
                    AlertHelper.createAlert(title: "Error", message: parsedError, view: self)
                }
            }
            else {
                if let accounts = objects {
                    for account in accounts {
                        
                        self.accounts.removeAll()
                        
                        let query = PFQuery(className: "Expense")
                        var totalExpense = 0.0
                        
                        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
                        query.whereKey("account_pointer", equalTo: account)
                        query.whereKey("month", equalTo: forMonth)
                        query.whereKey("year", equalTo: forYear)
                        
                        query.findObjectsInBackground(block: { (objects, error) in
                            if error != nil {
                                DispatchQueue.main.async {
                                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                                    let parsedError = ErrorHelper.parseError(defaultMessage: "Getting expenses failed. Please try again later.", error: error)
                                    AlertHelper.createAlert(title: "Error", message: parsedError, view: self)
                                }
                            }
                            else {
                                if let expenses = objects {
                                    for expense in expenses {
                                        totalExpense += expense["amount"] as! Double
                                    }
                                    
                                    if totalExpense > 0 {
                                        account["totalExpense"] = totalExpense
                                        self.accounts.append(account)
                                    }
                                }
                                //if last category has been saved, sort the total amount per category then reload table
                                if account == accounts.last {
                                    self.accounts.sort(by: { (accountOne, accountTwo) -> Bool in
                                        return accountOne["totalExpense"] as! Double > accountTwo["totalExpense"] as! Double
                                    })
                                    NotificationCenter.default.post(name: self.gotTotalExpenseByAccountNotification, object: nil)
                                }
                            }
                        })//end find expenses
                    }
                }
            }
        }

    }
    //gets total expenses per category for a certain month and year
    func setTotalExpensesByCategory(forMonth: Int, forYear: Int) {
        let query = PFQuery(className: "Category")
        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        
        query.findObjectsInBackground { (objects, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    let parsedError = ErrorHelper.parseError(defaultMessage: "Getting categories failed. Please try again later.", error: error)
                    AlertHelper.createAlert(title: "Error", message: parsedError, view: self)
                }
            }
            else {
                if let categories = objects {
                    for category in categories {
                        
                        self.categories.removeAll()
                        
                        let query = PFQuery(className: "Expense")
                        var totalExpense = 0.0
                        
                        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
                        query.whereKey("category_pointer", equalTo: category)
                        query.whereKey("month", equalTo: forMonth)
                        query.whereKey("year", equalTo: forYear)
                        
                        query.findObjectsInBackground(block: { (objects, error) in
                            if error != nil {
                                DispatchQueue.main.async {
                                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                                    let parsedError = ErrorHelper.parseError(defaultMessage: "Getting expenses failed. Please try again later.", error: error)
                                    AlertHelper.createAlert(title: "Error", message: parsedError, view: self)
                                }
                            }
                            else {
                                if let expenses = objects {
                                    for expense in expenses {
                                        totalExpense += expense["amount"] as! Double
                                    }
                                    if totalExpense > 0 {
                                        category["totalExpense"] = totalExpense
                                        self.categories.append(category)
                                    }
                                }
                                //if last category has been saved, sort the total amount per category then reload table
                                if category == categories.last {
                                    self.categories.sort(by: { (categoryOne, categoryTwo) -> Bool in
                                        return categoryOne["totalExpense"] as! Double > categoryTwo["totalExpense"] as! Double
                                    })
                                    NotificationCenter.default.post(name: self.gotTotalExpenseByCategoryNotification, object: nil)
                                }
                                
                            }
                        })//end find expenses
                    }
                }
            }
        }
    }

    //gets total expenses per category for a year
    func setTotalExpensesByCategory(forYear: Int) {
        let query = PFQuery(className: "Category")
        
        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        
        query.findObjectsInBackground { (objects, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    let parsedError = ErrorHelper.parseError(defaultMessage: "Getting categories failed. Please try again later.", error: error)
                    AlertHelper.createAlert(title: "Error", message: parsedError, view: self)
                }
            }
            else {
                if let categories = objects {
                    for category in categories {
                        
                        self.categories.removeAll()
                        
                        let query = PFQuery(className: "Expense")
                        var totalExpense = 0.0
                        
                        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
                        query.whereKey("category_pointer", equalTo: category)
                        query.whereKey("year", equalTo: forYear)
                        
                        query.findObjectsInBackground(block: { (objects, error) in
                            if error != nil {
                                DispatchQueue.main.async {
                                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                                    let parsedError = ErrorHelper.parseError(defaultMessage: "Getting expenses failed. Please try again later.", error: error)
                                    AlertHelper.createAlert(title: "Error", message: parsedError, view: self)
                                }
                            }
                            else {
                                if let expenses = objects {
                                    for expense in expenses {
                                        totalExpense += expense["amount"] as! Double
                                    }
                                    
                                    if totalExpense > 0 {
                                        category["totalExpense"] = totalExpense
                                        self.categories.append(category)
                                    }
                                }
                                //if last category has been saved, sort the total amount per category then reload table
                                if category == categories.last {
                                    self.categories.sort(by: { (categoryOne, categoryTwo) -> Bool in
                                        return categoryOne["totalExpense"] as! Double > categoryTwo["totalExpense"] as! Double
                                    })
                                    NotificationCenter.default.post(name: self.gotTotalExpenseByCategoryNotification, object: nil)
                                }
                                
                            }
                        })//end find expenses
                    }
                }
            }
        }
    }
    
    //get total expense for a certain year
    func getTotalExpenses(forYear: Int) {
        let query = PFQuery(className: "Expense")
        
        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        query.whereKey("year", equalTo: forYear)
        query.limit = 1000
        query.findObjectsInBackground { (objects, error) in
            if error != nil {
                DispatchQueue.main.async {
                    let parsedError = ErrorHelper.parseError(defaultMessage: "Getting expenses failed. Please try again later.", error: error)
                    AlertHelper.createAlert(title: "Error", message: parsedError, view: self)
                }
            }
            else {
                self.totalExpense = 0.0
                if let expenses = objects {
                    for expense in expenses {
                        self.totalExpense += expense["amount"] as! Double
                    }
                }
                if self.totalExpense == 0 {
                    DispatchQueue.main.async {
                        self.loadingHelper.stopActivityIndicator(uiView: self.view)
                        AlertHelper.createAlert(title: "No Transactions", message: "There are no recorded transactions for this time period", view: self)
                    }
                }
                NotificationCenter.default.post(name: self.gotTotalExpenseNotification, object: nil)
            }
        }

    }
    
    //get total expenses for a month and year
    func getTotalExpenses(forMonth: Int, forYear: Int) {
        let query = PFQuery(className: "Expense")
        
        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        query.whereKey("month", equalTo: forMonth)
        query.whereKey("year", equalTo: forYear)
        
        query.findObjectsInBackground { (objects, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.loadingHelper.stopActivityIndicator(uiView: self.view)
                    let parsedError = ErrorHelper.parseError(defaultMessage: "Getting expenses failed. Please try again later.", error: error)
                    AlertHelper.createAlert(title: "Error", message: parsedError, view: self)
                }
            }
            else {
                if let expenses = objects {
                    //if yearly reports is viewed
                    if self.isYearlyMode {
                        var totalMonthlyExpense = 0.0
                        
                        for expense in expenses {
                            totalMonthlyExpense += expense["amount"] as! Double
                        }
                        
                        self.totalExpensesPerMonth[forMonth] = totalMonthlyExpense
                        
                        //if last month total expense has been saved, reload table data
                        if forMonth == 12 {
                            NotificationCenter.default.post(name: self.gotTotalExpensePerMonthNotification, object: nil)
                        }
                    }
                    //if monthly reports is viewed
                    else {
                        self.totalExpense = 0.0
                        for expense in expenses {
                            self.totalExpense += expense["amount"] as! Double
                        }
                        if self.totalExpense == 0 {
                            DispatchQueue.main.async {
                                self.loadingHelper.stopActivityIndicator(uiView: self.view)
                                AlertHelper.createAlert(title: "No Transactions", message: "There are no recorded transactions for this time period", view: self)
                            }
                        }
                        NotificationCenter.default.post(name: self.gotTotalExpenseNotification, object: nil)
                    }
                }
            }
        }
    }
    
    //get total expense for a year separated in months
    func getTotalMonthlyExpenses(forYear: Int) {
        for i in 0...12 {
            getTotalExpenses(forMonth: i, forYear: forYear)
        }
    }
    
    //# MARK: - IBAction Methods
    @IBAction func cancel(_ sender: AnyObject) {
        NotificationCenter.default.removeObserver(self, name: gotTotalExpenseNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: gotTotalExpenseByCategoryNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: gotTotalExpenseByAccountNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: gotTotalExpensePerMonthNotification, object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    //# MARK: - TableView Methods
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale.current
        let amountText = numberFormatter.string(from: totalExpense as NSNumber)
        
        var dataPoints = [Double]()
        var dataLabels = [String]()
        var customLegendEntries = [LegendEntry]()
        
        //1st cell in table is month year or year selector
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "monthSelector", for: indexPath) as! MonthYearPickerTableViewCell
            
            //if yearly reports mode
            if isYearlyMode {
                cell.monthYearSelectorTextField.inputView = yearPicker
                cell.monthYearSelectorTextField.text = String(selectedYear)
            }
            //if monthly report mode
            else {
                let monthYearPicker = MonthYearPickerView()
                
                cell.monthYearSelectorTextField.inputView = monthYearPicker
                
                cell.monthYearSelectorTextField.text = String(format: "%02d/%d", selectedMonth, selectedYear)
                
                monthYearPicker.onDateSelected = { (month: Int, year: Int) in
                    let string = String(format: "%02d/%d", month, year)
                    cell.monthYearSelectorTextField.text = string
                    self.selectedMonth = month
                    self.selectedYear = year
                    self.generateMonthlyReports(forMonth: month, forYear: year)
                }
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.accessoryType = .disclosureIndicator
            
            return cell
        }
        //second cell is total expense for time period
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "totalExpense", for: indexPath) as! TotalExpensesTableViewCell

            cell.totalExpenseAmountLabel.text = amountText

            return cell
        }
        //3rd cell is pie chart of expense by account or pie chart of expense by category in a year if yearly mode
        else if indexPath.row == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "accountPieChartCell", for: indexPath) as! AccountPieChartTableViewCell
            
            if isYearlyMode {
                if categories.count > 0 {
                    dataPoints.removeAll()
                    dataLabels.removeAll()
                    
                    for i in 0...categories.count - 1 {
                        dataPoints.append(categories[i]["totalExpense"] as! Double)
                        dataLabels.append(categories[i]["name"] as! String)
                    }
                    
                    customLegendEntries = ChartHelper.createCustomLegendEntries(dataPoints: dataPoints, dataLabels: dataLabels, labelColors: ChartHelper.colors, percentDivisor: totalExpense)
                    
                    ChartHelper.createPieChart(dataPoints: dataPoints, dataLabels: dataLabels, pieChartView: cell.pieChartView, customLegendEntries: customLegendEntries, labelColors: ChartHelper.colors, centerText: amountText!, chartDescription: "Expense by Category")
                    
                }
                else {
                    //clear chart if no expense for the month
                    cell.pieChartView.clear()
                }
            }
            else {
                if accounts.count > 0 {
                    dataPoints.removeAll()
                    dataLabels.removeAll()
                    
                    for i in 0...accounts.count - 1 {
                        dataPoints.append(accounts[i]["totalExpense"] as! Double)
                        dataLabels.append(accounts[i]["name"] as! String)
                    }
                    
                    customLegendEntries = ChartHelper.createCustomLegendEntries(dataPoints: dataPoints, dataLabels: dataLabels, labelColors: ChartHelper.colors, percentDivisor: totalExpense)
                    
                    ChartHelper.createPieChart(dataPoints: dataPoints, dataLabels: dataLabels, pieChartView: cell.pieChartView, customLegendEntries: customLegendEntries, labelColors: ChartHelper.colors, centerText: amountText!, chartDescription: "Expense by Account")
                }
                else {
                    cell.pieChartView.clear()
                }
            }
            return cell
        }
        //4th cell is monthly expense by category or monthly expenses in a year graph if yearly mode
        else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "pieChartCell", for: indexPath) as! ChartTableViewCell
            
            //if yearly report mode, bar chart
            if isYearlyMode {
                cell.barChartView.isHidden = false
                cell.pieChartView.isHidden = true
                
                let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
                let colors = randomColors(12, hue: .random, luminosity: .light)
                var dataEntries: [BarChartDataEntry] = []
                var legendEntries: [LegendEntry] = []
               
                for i in 1...months.count {
                    let dataEntry = BarChartDataEntry(x: Double(i), yValues: [totalExpensesPerMonth[i]!], label: months[i - 1])
                    let legendEntry = LegendEntry()
                    legendEntry.label = months[i - 1]
                    legendEntry.formColor = colors[i - 1]
                    legendEntry.form = .circle
                    dataEntries.append(dataEntry)
                    legendEntries.append(legendEntry)
                }
                
                let barCharDataSet = BarChartDataSet(values: dataEntries, label: "Expenses per month")
                let barChartData = BarChartData(dataSet: barCharDataSet as IChartDataSet?)
                
                barCharDataSet.colors = colors
                cell.barChartView.legend.setCustom(entries: legendEntries)
                cell.barChartView.xAxis.drawGridLinesEnabled = false
                cell.barChartView.xAxis.axisMinimum = 0.0
                cell.barChartView.leftAxis.drawGridLinesEnabled = false
                cell.barChartView.rightAxis.drawGridLinesEnabled = false
                cell.barChartView.chartDescription?.text = "Expenses per month"
                cell.barChartView.data = barChartData
                cell.barChartView.animate(xAxisDuration: 1.5, yAxisDuration: 1.5)
            }
            // if monthly report mode, pie chart
            else {
                if categories.count > 0 {
                    cell.barChartView.isHidden = true
                    cell.pieChartView.isHidden = false
                    
                    dataPoints.removeAll()
                    dataLabels.removeAll()
                    
                    for i in 0...categories.count - 1 {
                        dataPoints.append(categories[i]["totalExpense"] as! Double)
                        dataLabels.append(categories[i]["name"] as! String)
                    }
                    
                    customLegendEntries = ChartHelper.createCustomLegendEntries(dataPoints: dataPoints, dataLabels: dataLabels, labelColors: ChartHelper.colors, percentDivisor: self.totalExpense)
                    
                    ChartHelper.createPieChart(dataPoints: dataPoints, dataLabels: dataLabels, pieChartView: cell.pieChartView, customLegendEntries: customLegendEntries, labelColors: ChartHelper.colors, centerText: amountText!, chartDescription: "Expense by Category")
                }
                else {
                    //clear chart if no expense for the month
                    cell.pieChartView.clear()
                }
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ExpenseByCategoryTableViewCell
            
            if categories.count > 0 {
                let amountText = numberFormatter.string(from: categories[indexPath.row - 4]["totalExpense"] as! NSNumber)
                
                cell.categoryLabel.text = categories[indexPath.row - 4]["name"] as! String?
                cell.amountLabel.text = amountText
                cell.colorLabel.mainColor = ChartHelper.colors[indexPath.row - 4]
                cell.colorLabel.layer.sublayers?.forEach({ (sublayer) in
                    sublayer.removeFromSuperlayer()
                })
                cell.colorLabel.draw(CGRect(x: 0, y: 0, width: cell.colorLabel.frame.width, height: cell.colorLabel.frame.height))
                cell.setNeedsDisplay()
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //static 3 cells + number of categories found
        return 4 + categories.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        
        if indexPath.row > 3 {
            selectedCategoryObject = categories[indexPath.row - 4]
            selectedCategory = categories[indexPath.row - 4]["name"] as! String
            performSegue(withIdentifier: "showExpenses", sender: self)
        }
    }
    
    //# MARK: - PickerView Methods
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yearPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(yearPickerData[row])
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedYear = yearPickerData[row]
        generateYearlyReports(forYear: selectedYear)
    }

    //# MARK: - Touch Methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //# MARK: - View Methods
    override func viewDidAppear(_ animated: Bool) {
        if isYearlyMode {
            generateYearlyReports(forYear: selectedYear)
        }
        else {
            generateMonthlyReports(forMonth: selectedMonth, forYear: selectedYear)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        let currentComponents = currentCalendar.dateComponents([.month, .year], from: date)
        selectedMonth = currentComponents.month!
        selectedYear = currentComponents.year!
        
        yearPicker.delegate = self
        
        //set how many years available in yearly reports
        for i in selectedYear - 3...selectedYear {
            yearPickerData.append(i)
        }
        
        yearPickerData.sort()
        
        //init total expense per month to 0 to avoid index error
        for i in 0...12 {
            totalExpensesPerMonth[i] = 0
        }
        
        if isYearlyMode {
            navigationBar.topItem?.title = "Yearly Reports"
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchReportsNotifications(notification:)), name: gotTotalExpenseNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.catchReportsNotifications(notification:)), name: gotTotalExpenseByCategoryNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchReportsNotifications(notification:)), name: gotTotalExpenseByAccountNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchReportsNotifications(notification:)), name: gotTotalExpensePerMonthNotification, object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
         dashboardDelegate?.reloadDashboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showExpenses" {
            let destinationController = segue.destination as! ExpenseForCategoryViewController
            
            destinationController.selectedCategoryObject = selectedCategoryObject
            destinationController.selectedCategory = selectedCategory
            destinationController.selectedYear = selectedYear
            destinationController.selectedMonth = selectedMonth
            destinationController.isYearlyMode = isYearlyMode
        }
    }
}
