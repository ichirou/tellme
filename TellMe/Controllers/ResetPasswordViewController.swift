//
//  ResetPasswordViewController.swift
//  TellMe
//
//  Created by Richmond Ko on 30/09/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Parse

class ResetPasswordViewController: UIViewController {

    var loadingHelper = LoadingHelper()
    
    @IBOutlet var navigationBarItem: UINavigationItem!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var emailTextField: UITextField!
    
    @IBAction func cancelResetPassword(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //reset password function
    @IBAction func resetPassword(_ sender: AnyObject) {
        loadingHelper.showActivityIndicatory(uiView: self.view, message: "Loading...")
        errorLabel.text = ""
        PFUser.requestPasswordResetForEmail(inBackground: emailTextField.text!) { (success, error) in
            if success {
                DispatchQueue.main.async {
                    self.errorLabel.text = "Please check your email for password reset instructions."
                }
            }
            else if error != nil {
                DispatchQueue.main.async {
                    ErrorHelper.setErrorLabel(defaultMessage: "Password reset failed, please try again later.", errorLabel: self.errorLabel, error: error)
                }
            }
            DispatchQueue.main.async {
                self.loadingHelper.stopActivityIndicator(uiView: self.view)
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        BorderHelper.addBottomBorder(view: emailTextField, color: UIColor.gray)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
