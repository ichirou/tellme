//
//  SpeechRecognitionViewController.swift
//  TellMe
//
//  Created by Richmond Ko on 22/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Speech

class SpeechRecognitionViewController: UIViewController, SFSpeechRecognizerDelegate {
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    var finalRecognizedString = ""
    var speechData = [String: String]()
    var isResetMode = false
    var doneButtonColor = UIColor()
    var delegate: PassDataBackDelegate?
    
    @IBOutlet var speechTextView: UITextView!
    @IBOutlet var recordingButton: UIButton!
    @IBOutlet var doneButton: UIButton!
    
    @IBAction func done(_ sender: AnyObject) {
        print(finalRecognizedString)
        //let mockstring = "I spent 2500.00 for Battlefield I under asdasd using asfasda"
        //delegate?.setSpeechRecognitionData(data: SpeechRecognitionHelper.recognizeDataFromText(text: mockstring))
        delegate?.setSpeechRecognitionData(data: SpeechRecognitionHelper.recognizeDataFromText(text: finalRecognizedString))
        delegate?.checkExistingCategoriesAndAccounts()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func record(_ sender: AnyObject) {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            recordingButton.isEnabled = false
            doneButton.isEnabled = false
            doneButton.backgroundColor = UIColor.gray
            recordingButton.setTitle("Stopping", for: .disabled)
        } else {
            try! startRecording()
            recordingButton.setTitle("Stop recording", for: [])
        }
    }
    
    @IBAction func cancel(_ sender: AnyObject) {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    private func startRecording() throws {
        
        // Cancel the previous task if it's running.
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSessionCategoryRecord)
        try audioSession.setMode(AVAudioSessionModeMeasurement)
        try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let inputNode = audioEngine.inputNode else { fatalError("Audio engine has no input node") }
        guard let recognitionRequest = recognitionRequest else { fatalError("Unable to created a SFSpeechAudioBufferRecognitionRequest object") }
        
        // Configure request so that results are returned before audio recording is finished
        recognitionRequest.shouldReportPartialResults = true
        
        // A recognition task represents a speech recognition session.
        // We keep a reference to the task so that it can be cancelled.
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in
            var isFinal = false
            
            if let result = result {
                self.speechTextView.text = result.bestTranscription.formattedString
                self.finalRecognizedString = result.bestTranscription.formattedString
                isFinal = result.isFinal
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.recordingButton.isEnabled = true
                self.doneButton.isEnabled = true
                self.doneButton.backgroundColor = self.doneButtonColor
                self.recordingButton.setTitle("Start Recording", for: [])
            }
        }
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        try audioEngine.start()
        
        speechTextView.text = "(Go ahead, I'm listening)"
    }
    
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            recordingButton.isEnabled = true
            recordingButton.setTitle("Start Recording", for: [])
        } else {
            recordingButton.isEnabled = false
            recordingButton.setTitle("Recognition not available", for: .disabled)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        speechRecognizer.delegate = self
        
        SFSpeechRecognizer.requestAuthorization { authStatus in
            /*
             The callback may not be called on the main thread. Add an
             operation to the main queue to update the record button's state.
             */
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.recordingButton.isEnabled = true
                    self.record(self)
                    
                case .denied:
                    self.recordingButton.isEnabled = false
                    self.recordingButton.setTitle("User denied access to speech recognition", for: .disabled)
                    
                case .restricted:
                    self.recordingButton.isEnabled = false
                    self.recordingButton.setTitle("Speech recognition restricted on this device", for: .disabled)
                    
                case .notDetermined:
                    self.recordingButton.isEnabled = false
                    self.recordingButton.setTitle("Speech recognition not yet authorized", for: .disabled)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recordingButton.isEnabled = false
        doneButton.isEnabled = false
        doneButtonColor = doneButton.backgroundColor!
        doneButton.backgroundColor = UIColor.gray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
