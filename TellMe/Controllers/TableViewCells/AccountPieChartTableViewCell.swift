//
//  AccountPieChartTableViewCell.swift
//  TellMe
//
//  Created by Richmond Ko on 18/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Charts

class AccountPieChartTableViewCell: UITableViewCell {

    @IBOutlet var pieChartView: PieChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
