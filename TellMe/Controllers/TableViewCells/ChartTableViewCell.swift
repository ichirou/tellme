//
//  PieChartTableViewCell.swift
//  TellMe
//
//  Created by Richmond Ko on 11/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit
import Charts

class ChartTableViewCell: UITableViewCell {
    
    @IBOutlet var pieChartView: PieChartView!
    @IBOutlet var barChartView: BarChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
