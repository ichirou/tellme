//
//  ExpenseByCategoryTableViewCell.swift
//  TellMe
//
//  Created by Richmond Ko on 10/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit

class ExpenseByCategoryTableViewCell: UITableViewCell {

    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var colorLabel: Dot!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
