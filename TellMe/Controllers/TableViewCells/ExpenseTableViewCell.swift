//
//  ExpenseTableViewCell.swift
//  TellMe
//
//  Created by Richmond Ko on 28/09/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit

class ExpenseTableViewCell: UITableViewCell {

    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var accountLabel: UILabel!
    @IBOutlet var expenseLabel: UILabel!
    @IBOutlet var accountIcon: UIImageView!
    @IBOutlet var categoryIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
