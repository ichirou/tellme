//
//  MonthYearPickerTableViewCell.swift
//  TellMe
//
//  Created by Richmond Ko on 11/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit

class MonthYearPickerTableViewCell: UITableViewCell {

    @IBOutlet var monthYearSelectorTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
