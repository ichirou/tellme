//
//  TotalExpensesTableViewCell.swift
//  TellMe
//
//  Created by Richmond Ko on 08/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import UIKit

class TotalExpensesTableViewCell: UITableViewCell {

    @IBOutlet var totalExpenseAmountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
