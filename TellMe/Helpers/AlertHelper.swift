//
//  AlertHelper.swift
//  TellMe
//
//  Created by Richmond Ko on 30/09/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import Foundation
import UIKit

class AlertHelper {
    static func createAlert(title: String, message: String, view: UIViewController) {
        
        var alertStyle = UIAlertControllerStyle.actionSheet
    
        if UIDevice.current.userInterfaceIdiom == .pad {
            alertStyle = UIAlertControllerStyle.alert
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: alertStyle)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
        }))
        
        view.present(alertController, animated: true, completion: nil)
    }
    
    static func createDeleteConfirmation(title: String, message: String, view: UIViewController, funcIfOK: @escaping (Int) -> Void, selectedRow: Int) {
        var alertStyle = UIAlertControllerStyle.actionSheet
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alertStyle = UIAlertControllerStyle.alert
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: alertStyle)
        
        let okActionHandler = { (action: UIAlertAction!) -> Void in
            funcIfOK(selectedRow)
        }
        
        let okAction = UIAlertAction(title: "OK", style: .destructive, handler: okActionHandler)
        
        alertController.addAction(okAction)
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
        }))
        
        view.present(alertController, animated: true, completion: nil)
    }
    
    static func createMultipleDeleteAlert(title: String, message: String, view: UIViewController, tableView: UITableView, funcIfOK: @escaping (String, [String], @escaping () -> Void, UIViewController) -> Void, className: String, objectIds: [String], funcToExecuteAfterDelete: @escaping () -> Void, uiViewController: UIViewController) {
        var alertStyle = UIAlertControllerStyle.actionSheet
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alertStyle = UIAlertControllerStyle.alert
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: alertStyle)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { (action) in
            funcIfOK(className, objectIds, funcToExecuteAfterDelete, uiViewController)
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            tableView.reloadData()
        }))
        
        view.present(alertController, animated: true, completion: nil)
    }
}
