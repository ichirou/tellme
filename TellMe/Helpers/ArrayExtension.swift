//
//  ArrayExtension.swift
//  TellMe
//
//  Created by Richmond Ko on 13/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func removeObject(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}

extension String {
    func index(of string: String) -> String.Index? {
        return range(of: string)?.lowerBound
    }
}
