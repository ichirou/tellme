//
//  BorderHelper.swift
//  TellMe
//
//  Created by Richmond Ko on 04/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import Foundation
import UIKit

class BorderHelper {
    static func addBottomBorder(view: UIView, color: UIColor) {
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = color.cgColor
        border.frame = CGRect(x: 0, y: view.frame.size.height - borderWidth, width:  view.frame.size.width, height: view.frame.size.height)
        
        border.borderWidth = borderWidth
        
        view.layer.addSublayer(border)
        view.layer.masksToBounds = true
    }
}
