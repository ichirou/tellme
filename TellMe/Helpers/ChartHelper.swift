//
//  ChartHelper.swift
//  TellMe
//
//  Created by Richmond Ko on 18/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import Foundation
import Charts
import Parse
import RandomColorSwift

class ChartHelper {
    
    static let colors = ChartColorTemplates.vordiplom() + ChartColorTemplates.pastel() + ChartColorTemplates.pastel() + ChartColorTemplates.liberty() + ChartColorTemplates.colorful()
    
    //static let colors = randomColors(20, hue: .random, luminosity: .light)
    
    static func createPieChart(dataPoints: [Double], dataLabels: [String], pieChartView: PieChartView, customLegendEntries: [LegendEntry], labelColors: [UIColor], centerText: String, chartDescription: String) {
        var dataEntries: [PieChartDataEntry] = []
        
        //formatter for pie chart % value
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .percent
        numberFormatter.maximumFractionDigits = 0
        numberFormatter.multiplier = 1.0
        numberFormatter.percentSymbol = "%"
        
        for i in 0...dataPoints.count - 1 {
            let dataEntry = PieChartDataEntry(value: dataPoints[i], label: dataLabels[i])
            dataEntries.append(dataEntry)
        }
        
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        let pieChartData = PieChartData(dataSet: pieChartDataSet as IChartDataSet?)
        pieChartDataSet.colors = labelColors
        pieChartDataSet.valueTextColor = UIColor.black
        pieChartDataSet.valueFont = UIFont.systemFont(ofSize: 11)
        pieChartDataSet.sliceSpace = 1.0
        pieChartDataSet.selectionShift = 5.0
        pieChartData.setValueFormatter(DefaultValueFormatter.init(formatter: numberFormatter))
        pieChartView.legend.setCustom(entries: customLegendEntries)
        pieChartView.legend.font = UIFont.boldSystemFont(ofSize: 11)
        pieChartView.legend.textColor = UIColor.darkGray
        pieChartView.legend.horizontalAlignment = Legend.HorizontalAlignment.center
        pieChartView.chartDescription?.text = chartDescription
        pieChartView.chartDescription?.yOffset = 10
        pieChartView.usePercentValuesEnabled = true
        pieChartView.holeRadiusPercent = 0.40
        pieChartView.transparentCircleRadiusPercent = 0.45
        pieChartView.legend.verticalAlignment = .top
        pieChartView.legend.horizontalAlignment = .left
        pieChartView.legend.orientation = Legend.Orientation.vertical
        pieChartView.legend.drawInside = false
        pieChartView.animate(xAxisDuration: 1.5)
        pieChartView.drawEntryLabelsEnabled = false
        pieChartView.centerText = centerText
        pieChartView.data = pieChartData
    }
    
    static func createCustomLegendEntries(dataPoints: [Double], dataLabels: [String], labelColors: [UIColor], percentDivisor: Double) -> [LegendEntry] {
        
        var customLegendEntries = [LegendEntry]()
        
        //formatter for legend % value
        let percentFormatter = NumberFormatter()
        percentFormatter.numberStyle = .percent
        percentFormatter.maximumFractionDigits = 0
        percentFormatter.percentSymbol = "%"
        
        for i in 0...dataPoints.count - 1 {
            let legendEntry = LegendEntry()
            let percentValue = dataPoints[i] / percentDivisor
            legendEntry.label = dataLabels[i] + " " + percentFormatter.string(from: percentValue as NSNumber)!
            legendEntry.form = Legend.Form.circle
            legendEntry.formColor = labelColors[i]
            customLegendEntries.append(legendEntry)
        }
        
        return customLegendEntries
    }
}
