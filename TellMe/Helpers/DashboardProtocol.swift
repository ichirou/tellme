//
//  DashboardProtocol.swift
//  TellMe
//
//  Created by Richmond Ko on 28/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import Foundation

protocol DashboardDelegate {
    func reloadDashboard()
}
