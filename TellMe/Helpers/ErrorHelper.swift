//
//  ErrorHelper.swift
//  TellMe
//
//  Created by Richmond Ko on 02/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import Foundation
import UIKit

class ErrorHelper{
    static func setErrorLabel(defaultMessage: String, errorLabel: UILabel, error: Error?) {
        var errorMessage = defaultMessage
        
        let error = error as? NSError
        
        if let parsedError = error?.userInfo["error"] as? String {
            errorMessage = parsedError
        }
        
        errorLabel.text = errorMessage
    }
    
    static func parseError(defaultMessage: String, error: Error?) -> String {
        var errorMessage = defaultMessage
        
        let error = error as? NSError
        
        if let parsedError = error?.userInfo["error"] as? String {
            errorMessage = parsedError
        }
        
        return errorMessage
    }
}
