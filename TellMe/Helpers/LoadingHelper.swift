//
//  LoadingHelper.swift
//  TellMe
//
//  Created by Richmond Ko on 29/09/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import Foundation
import UIKit

class LoadingHelper {
    
    var loadingView = UIView()
    var actInd = UIActivityIndicatorView()
    var loadingLabel = UILabel()
    
    func showActivityIndicatory(uiView: UIView, message: String) {
        loadingLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
        loadingLabel.text = message
        loadingLabel.textColor = UIColor.white
        loadingLabel.adjustsFontSizeToFitWidth = true
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        loadingView.layer.cornerRadius = 15
        loadingView.tag = 3
        
        actInd.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.white
        
        loadingView.addSubview(actInd)
        loadingView.addSubview(loadingLabel)
        uiView.addSubview(loadingView)
        
        actInd.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator(uiView: UIView) {
        actInd.stopAnimating()
        for view in uiView.subviews {
            if view.tag == 3 {
                view.removeFromSuperview()
            }
        }
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func showLoadingWithoutIgnoringInteractionEvents(uiView: UIView, message: String) {
        loadingLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
        loadingLabel.text = message
        loadingLabel.textColor = UIColor.white
        loadingLabel.adjustsFontSizeToFitWidth = true
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        loadingView.layer.cornerRadius = 15
        loadingView.tag = 3
        
        actInd.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.white
        
        loadingView.addSubview(actInd)
        loadingView.addSubview(loadingLabel)
        uiView.addSubview(loadingView)
        
        actInd.startAnimating()
    }
    
    func stopLoading(uiView: UIView) {
        actInd.stopAnimating()
        for view in uiView.subviews {
            if view.tag == 3 {
                view.removeFromSuperview()
            }
        }
    }
}
