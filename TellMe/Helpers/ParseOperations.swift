//
//  ParseOperations.swift
//  TellMe
//
//  Created by Richmond Ko on 13/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import Foundation
import Parse

class ParseOperations {
    
    static func deleteObjectsWith(className: String, objectIds: [String], funcToExecuteAfterDelete: @escaping () -> Void, viewController: UIViewController) {
        let loadingHelper = LoadingHelper()
        loadingHelper.showActivityIndicatory(uiView: viewController.view, message: "Deleting...")
        
        let query = PFQuery(className: className)
        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        query.whereKey("objectId", containedIn: objectIds)
        
        query.findObjectsInBackground(block: { (objects, error) in
            if error != nil {
                DispatchQueue.main.async {
                    let errorMessage = ErrorHelper.parseError(defaultMessage: "Finding " + className + " to delete failed, please try again later.", error: error)
                    loadingHelper.stopActivityIndicator(uiView: viewController.view)
                    
                    AlertHelper.createAlert(title: "Error", message: errorMessage, view: viewController)
                }
            }
            else {
                if let objects = objects {
                    PFObject.deleteAll(inBackground: objects, block: { (success, error) in
                        if error != nil {
                            DispatchQueue.main.async {
                                loadingHelper.stopActivityIndicator(uiView: viewController.view)
                                let errorMessage = ErrorHelper.parseError(defaultMessage: "Delete " + className + " failed, please try again later.", error: error)
                                
                                AlertHelper.createAlert(title: "Error", message: errorMessage, view: viewController)
                            }
                        }
                        else if success {
//                            DispatchQueue.main.async {
//                                AlertHelper.createAlert(title: "Success", message: "Items deleted.", view: viewController)
//                            }
                        }
                        DispatchQueue.main.async {
                            loadingHelper.stopActivityIndicator(uiView: viewController.view)
                        }
                        funcToExecuteAfterDelete()
                    })
                }
            }
            
        })// end find objects in background
    }
}
