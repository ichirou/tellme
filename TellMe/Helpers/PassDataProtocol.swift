//
//  PassDataProtocol.swift
//  TellMe
//
//  Created by Richmond Ko on 04/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import Foundation
import UIKit
import Parse

protocol PassDataBackDelegate {
    func setSelectedAccountRow(value: Int)
    func setSelectedAccountButtonTitle(value: String)
    func setSelectedCategoryRow(value: Int)
    func setSelectedCategoryButtonTitle(value: String)
    func setSelectAccountButtonTextColor(color: UIColor)
    func setSelectCategoryButtonTextColor(color: UIColor)
    func setSpeechRecognitionData(data: [String: String])
    func setSelectedCategoryObject(category: PFObject)
    func setSelectedAccountObject(account: PFObject)
    func checkExistingCategoriesAndAccounts()
}
