//
//  SpeechRecognitionHelper.swift
//  TellMe
//
//  Created by Richmond Ko on 18/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import Foundation

class SpeechRecognitionHelper {
    
    static func removeLastCharacter(string: String) -> String {
        return string.substring(to: string.index(before: string.endIndex))
    }
    
    static func recognizeDataFromText(text: String) -> [String: String] {
        var words = text.components(separatedBy: " ")
        var data = [String: String] ()
        print(words)
        data["amount"] = ""
        data["description"] = ""
        data["category"] = ""
        data["account"] = ""
        
        for (index, word) in words.enumerated() {
            var newindex = index + 1
            
            if word == "spent" || word == "paid" || word == "spend" || word == "Spend" || word == "Spent"{
                if newindex < words.count {
                    let amountWithoutComma = words[index + 1].replacingOccurrences(of: ",", with: "")
                    if let parsedAmount = Double(amountWithoutComma){
                        data["amount"] = String(parsedAmount)
                    }
                }
            }
            if word == "for" || word == "at"{
                newindex = index + 1
                if newindex > words.count - 1 {
                    continue
                }
                if (words.contains("under")) {
                    while words[newindex] != "under" {
                        data["description"] = data["description"]! + words[newindex] + " "
                        newindex += 1
                    }
                }
                else if (words.contains("using")) {
                    while words[newindex] != "using" {
                        data["description"] = data["description"]! + words[newindex] + " "
                        newindex += 1
                    }
                }
                else {
                    while newindex < words.count {
                        data["description"] = data["description"]! + words[newindex] + " "
                        newindex += 1
                    }
                }
            }
            if word == "under" {
                newindex = index + 1
                if newindex > words.count - 1 {
                    continue
                }
                if (words.contains("using")) {
                    while words[newindex] != "using" {
                        data["category"] = data["category"]! + words[newindex] + " "
                        newindex += 1
                        if newindex > words.count - 1 {
                            break
                        }
                    }
                }
                else {
                    while newindex < words.count {
                        data["category"] = data["category"]! + words[newindex] + " "
                        newindex += 1
                    }
                }
            }
            if word == "using" {
                newindex = index + 1
                if newindex > words.count - 1 {
                    continue
                }
                print(newindex)
                if (words.contains("under")) {
                    while words[newindex] != "under" {
                        data["account"] = data["account"]! + words[newindex] + " "
                        newindex += 1
                        if newindex > words.count - 1 {
                            break
                        }
                    }
                }
                else {
                    while newindex < words.count {
                        data["account"] = data["account"]! + words[newindex] + " "
                        newindex += 1
                    }
                }
            }
        }
        
        if data["description"] != "" {
            data["description"] = self.removeLastCharacter(string: data["description"]!)
        }
        if data["account"] != "" {
            data["account"] = self.removeLastCharacter(string: data["account"]!)

        }
        if data["category"] != "" {
            data["category"] = self.removeLastCharacter(string: data["category"]!)
        }
        
        if (data["description"]?.contains("using"))! {
            if var descriptionString = data["description"]{
                let startIndex = descriptionString.index(descriptionString.index(of: "using")!, offsetBy: 0)
                let endIndex = descriptionString.index(descriptionString.endIndex, offsetBy: 0)
                let subrange = startIndex ..< endIndex
                descriptionString.removeSubrange(subrange)
                data["description"] = descriptionString
            }
        }
        
        print("amount: " + data["amount"]!)
        print("description: " + data["description"]!)
        print("account: " + data["account"]!)
        print("category: " + data["category"]! + "\n")
        
        return data
    }
}
