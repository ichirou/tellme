//
//  TestDataScript.swift
//  TellMe
//
//  Created by Richmond Ko on 10/10/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import Foundation
import Parse

class TestDataScript {
    
    public static func randomDateWithinDaysBeforeToday(_ days: Int) -> Date {
        let today = Date()
        let gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
        
        let r1 = arc4random_uniform(UInt32(days))
        let r2 = arc4random_uniform(UInt32(23))
        let r3 = arc4random_uniform(UInt32(59))
        let r4 = arc4random_uniform(UInt32(59))
        
        var offsetComponents = DateComponents()
        offsetComponents.day = Int(r1) * -1
        offsetComponents.hour = Int(r2)
        offsetComponents.minute = Int(r3)
        offsetComponents.second = Int(r4)
        
        guard let rndDate1 = gregorian.date(byAdding: offsetComponents, to: today) else {
            print("randoming failed")
            return today
        }
        return rndDate1
    }
    
    public static func randomDate() -> Date {
        let randomTime = TimeInterval(arc4random_uniform(UInt32.max))
        return Date(timeIntervalSince1970: randomTime)
    }
    
    static func createExpensesWith(description: String, account: PFObject, amount: Double, category: PFObject, date: Date) {
        let transaction = PFObject(className: "Expense")
        let calendarcomponents = Calendar.current.dateComponents([.day, .month, .year], from: date)
        let month = calendarcomponents.month!
        let year = calendarcomponents.year!

        transaction["owner"] = PFUser.current()?.objectId
        transaction["description"] = description
        transaction["amount"] = amount
        transaction["date"] = date
        transaction["canonicalDescription"] = description.lowercased()
        transaction["month"] = month
        transaction["year"] = year
        transaction["category_pointer"] = category
        transaction["account_pointer"] = account
        
        transaction.saveInBackground()
    }
    
    static func createExpenses() {
        //resetExpenses()
        
        let daysBefore = 1095
        
        var electronics = PFObject(className: "Category")
        var restaurant = PFObject(className: "Category")
        var food = PFObject(className: "Category")
        var transportation = PFObject(className: "Category")
        var utilities = PFObject(className: "Category")
        var clothing = PFObject(className: "Category")
        var entertainment = PFObject(className: "Category")
        var travel = PFObject(className: "Category")
        var health = PFObject(className: "Category")
        var investment = PFObject(className: "Category")
        var parking = PFObject(className: "Category")
        
        var mywallet = PFObject(className: "Account")
        var mycreditcard = PFObject(className: "Account")
        var mybankaccount = PFObject(className: "Account")
        
        let query = PFQuery(className: "Category")
        let accountQuery = PFQuery(className: "Account")
        
        accountQuery.whereKey("name", equalTo: "My Wallet")
        try! mywallet = accountQuery.getFirstObject()
        
        print(mywallet)
        
        accountQuery.whereKey("name", equalTo: "My Credit Card")
        try! mycreditcard = accountQuery.getFirstObject()
        
        accountQuery.whereKey("name", equalTo: "My Bank Account")
        try! mybankaccount = accountQuery.getFirstObject()
        
        query.whereKey("name", equalTo: "Electronics")
        try! electronics = query.getFirstObject()
        
        query.whereKey("name", equalTo: "Restaurant")
        try! restaurant = query.getFirstObject()
        
        query.whereKey("name", equalTo: "Food")
        try! food = query.getFirstObject()
        
        query.whereKey("name", equalTo: "Transportation")
        try! transportation = query.getFirstObject()
        
        query.whereKey("name", equalTo: "Utilities")
        try! utilities = query.getFirstObject()
        
        query.whereKey("name", equalTo: "Clothing")
        try! clothing = query.getFirstObject()
        
        query.whereKey("name", equalTo: "Entertainment")
        try! entertainment = query.getFirstObject()
        
        query.whereKey("name", equalTo: "Travel")
        try! travel = query.getFirstObject()
        
        query.whereKey("name", equalTo: "Health")
        try! health = query.getFirstObject()
        
        query.whereKey("name", equalTo: "Investment")
        try! investment = query.getFirstObject()
        
        query.whereKey("name", equalTo: "Parking")
        try! parking = query.getFirstObject()
        
        for _ in 0...50 {
            createExpensesWith(description: "Mac Book Air", account: mywallet, amount: 10000.00, category: electronics, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "Izakaya Kikufuji", account: mycreditcard, amount: 1852.23, category: restaurant, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "Jolibee", account: mywallet, amount: 280.00, category: food, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "Your Local", account: mywallet, amount: 2586.22, category: restaurant, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "Gas", account: mycreditcard, amount: 1560.59, category: transportation, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "Meralco", account: mywallet, amount: 5201.34, category: utilities, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "Uniqlo Suit", account: mywallet, amount: 2999.00, category: clothing, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "Train to Busan Movie", account: mywallet, amount: 500.00, category: entertainment, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "Davao Trip", account: mywallet, amount: 8000.00, category: travel, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "Gym Membership", account: mywallet, amount: 1500.00, category: health, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "COL Deposit", account: mybankaccount, amount: 8000.00, category: investment, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "KFC", account: mywallet, amount: 150.00, category: food, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "Starbucks", account: mywallet, amount: 280.00, category: food, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "Sushi Plate", account: mywallet, amount: 350.00, category: food, date: randomDateWithinDaysBeforeToday(daysBefore))
            createExpensesWith(description: "Parking", account: mywallet, amount: 100.00, category: parking, date: randomDateWithinDaysBeforeToday(daysBefore))
        }
    }
    
    static func createCategoryWith(name: String) {
        let category = PFObject(className: "Category")
        
        category["name"] = name
        category["owner"] = PFUser.current()?.objectId
        category["canonicalName"] = name.lowercased()
        category.saveInBackground()
    }
    
    static func createCategories() {
        //resetCategories()
        createCategoryWith(name: "Food")
        createCategoryWith(name: "Restaurant")
        createCategoryWith(name: "Transportation")
        createCategoryWith(name: "Utilities")
        createCategoryWith(name: "Parking")
        createCategoryWith(name: "Electronics")
        createCategoryWith(name: "Clothing")
        createCategoryWith(name: "Entertainment")
        createCategoryWith(name: "Travel")
        createCategoryWith(name: "Health")
        createCategoryWith(name: "Groceries")
        createCategoryWith(name: "Gifts")
        createCategoryWith(name: "Pets")
        createCategoryWith(name: "Children")
        createCategoryWith(name: "Education")
        createCategoryWith(name: "Investment")
        createCategoryWith(name: "Business")
        createCategoryWith(name: "Insurances")
        createCategoryWith(name: "Fees")
        createCategoryWith(name: "Others")
    }
    
    static func createAccountWith(name: String, type: String) {
        let account = PFObject(className: "Account")
        
        account["name"] = name
        account["type"] = type
        account["owner"] = PFUser.current()?.objectId
        
        account.saveInBackground()
    }
    
    static func createAccounts() {
        //resetAccounts()
        createAccountWith(name: "My Wallet", type: "Cash")
        createAccountWith(name: "My Credit Card", type: "Charge")
        createAccountWith(name: "My Bank Account", type: "Cash")
    }
    
    static func resetExpenses() {
        let query = PFQuery(className: "Expense")
        
        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        
        query.findObjectsInBackground { (objects, error) in
            if error != nil {
                
            }
            else {
                if let objects = objects {
                    PFObject.deleteAll(inBackground: objects)
                }
            }
        }
    }
    
    static func resetAccounts() {
        let query = PFQuery(className: "Account")
        
        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        
        query.findObjectsInBackground { (objects, error) in
            if error != nil {
                
            }
            else {
                if let objects = objects {
                    for object in objects {
                        object.deleteInBackground()
                    }
                }
            }
        }
    }
    
    static func resetCategories() {
        let query = PFQuery(className: "Category")
        
        query.whereKey("owner", equalTo: (PFUser.current()?.objectId!)!)
        
        query.findObjectsInBackground { (objects, error) in
            if error != nil {
                
            }
            else {
                if let objects = objects {
                    PFObject.deleteAll(inBackground: objects)
                }
            }
        }
    }
}
