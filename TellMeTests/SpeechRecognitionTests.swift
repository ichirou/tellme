//
//  SpeechRecognitionTests.swift
//  TellMe
//
//  Created by Richmond Ko on 04/11/2016.
//  Copyright © 2016 PenguinLabs. All rights reserved.
//

import XCTest
@testable import TellMe
import FirebaseCore

class SpeechRecognitionTests: XCTestCase {

//    texts.append("I spent 4999.12 for Single Origin under Food using my wallet")
//    texts.append("I spent 4999.12 for Single Origin under Food using bank account")
//    texts.append("I spent 4999.12 for Single Origin under Food using")
//    texts.append("I spent 4999.12 for Single Origin under Food")
//    texts.append("I paid 4999.12 for Single Origin under Food")
//    texts.append("I paid 4999.12 for Single Origin under Food and restaurant")
//    texts.append("paid 3112 for ipad air")
//    texts.append("")
//    texts.append("random gibberish that i said yesterday")
//    texts.append("random gibberish that i said yesterday for under")
//    texts.append("for random under gibberish that i said yesterday for under")
//    texts.append("I paid 4999.12")
//    texts.append("I paid for mcdonalds")
//    texts.append("something")
//    texts.append("spent")
//    texts.append("paid for")
//    texts.append("I spend 500 for shoes underclothes using my alarm")
//    texts.append("I spent 100 for san pellegrino using my wallet under food")
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFullPhrase() {
        let string = "I spent 4999.12 for Single Origin under Food using my wallet"
        let data = SpeechRecognitionHelper.recognizeDataFromText(text: string)
        
        XCTAssert(data["description"] == "Single Origin", "Got wrong description")
        XCTAssert(data["account"] == "my wallet", "Got wrong account")
        XCTAssert(data["category"] == "Food", "Got wrong category")
        XCTAssert(data["amount"] == "4999.12", "Got wrong acmount")
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
